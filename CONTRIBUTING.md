# Contributing to COVID-19 Support System
FreelyGive have made this repository public to aid quick sharing of the system
for others to use. Please feel free to make use of it and to contribute back
any improvements that you make.

### How to contribute
The easiest way to contribute is to fork the repository, make your changes and
then create a merge request back into this repository. This is documented in
the GitLab's [Project forking workflow](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html).

### Can this be done better?
Yes! However, due to the pressing need surrounding COVID-19, we have opted to
focus on getting the required features in place rather than making it easy for
the system to be used and extended by others. We hope to move towards a more
familiar module or install profile route soon.


### Getting started
You can get started quickly by using DDev as a local environment and installing
the site using existing config. Once you have [installed DDev](https://ddev.readthedocs.io/en/stable/#installation)

Make sure you have put your public SSH key into gitlab


you can use the following commands to have a running instance.


```shell
# Clone the repository.
git clone https://gitlab.com/freelygive/covid-support-system.git covid
cd covid

## Go into composer.json
Remove the line: 


        {
            "type": "vcs",
            "url": "https://github.com/FreelyGive/gdpr.git"
        },

# Ensure DDev is configured and settings files are created.
ddev config --docroot=web  --project-type=drupal8

# This repo is currently using an older version of composer
ddev config --composer-version 1

# Start the DDev containers.
ddev start

# Install dependencies.
ddev composer install

# Install Drupal. Make a note of the password generated.
ddev drush site-install --existing-config

# Optionally add the crm_indiv/staff/staff admin roles to admin user.
ddev drush user:role:add crm_indiv admin
ddev drush user:role:add staff admin
ddev drush user:role:add staff_admin admin
```

You will then be able to log in at https://covid.ddev.site.

To enable geocoding, you will need to add and configure a geocoder plugin. The
googlemaps plugin is enabled by default and an API key can be added by adding
the following to `web/sites/default/settings.local.php` or via instance
specific configuration (see
[Configuration management](#configuration-management)):

```php
$config['geocoder.settings']['plugins_options']['googlemaps']['apikey'] = 'API KEY';
```

### Configuration management
To provide a consistent set of base configuration whilst allowing for site
specific changes, we are using
[Configuration Split](https://drupal.org/project/config_split). All base
configuration is located in `config/sync`. There are two split configurations
provided:

- `dev`: This is for development only tools/configuration, such as Devel and
  config inspector.
- `instance`: This is for instance specific configuration, such as site name,
  email, themes & block configuration etc.

If you are working inside a DDev environment, the default will be to enable
`dev` and disable `instance`. Otherwise, the default is to enable `instance`
and disable `dev`. This can also be overridden in `settings.local.php` with:

```php
$config['config_split.config_split.dev']['status'] = TRUE;
$config['config_split.config_split.instance']['status'] = TRUE;
```

You can import/export configuration as you would normally (`drush cim` and
`drush cex` respectively) and it will import from/export to the correct
directories depending on the configuration of the split. You may need to
manually copy configuration back to `config/sync` if you intend a change in a
split area of configuration to go into the base system. You can also
temporarily disable the relevant split and export normally.

_Note: If you change the status of a split in `settings.local.php` you will
need to clear caches (`drush cr`) before importing/exporting configuration._

If you need to add additional configuration to a split, that can be done by
editing the split at `admin/config/development/configuration/config-split`.
- __Complete split:__ This is for configuration that should not be in the base
  system at all, e.g. local development modules or custom instance modules.
- __Conditional split:__ This is for configuration that has a default in the
  base system, but is allowed to vary per instances, e.g. site name and email.

##### Instance specific themes & modules
Enabling a themes and modules for a specific instance is a little more
involved. To enable a theme or module, you first need to copy
`config/sync/config_split.config_split.instance.yml` to
`config/instance/config_split.config_split.instance.yml` and then add the
theme/module manually to the instance specific version, for example:
```yaml
module:
  example_module: 0
theme:
  example_theme: 0
```


### Making Changes to our theme.
Most of what we do with themes should be self-explanatory to individuals who 
work with Drupal. We use the theme bootstrap_barrio which is a base theme for 
Drupal utilising bootstrap. We then take the Bootstrap 4 Barrio SASS subtheme 
and rename it and place the new theme in the custom themes folder.

The Barrior SASS subtheme almost exclusively uses SASS found in the scss folder.
You will need to run gulp whilst editing those files so that the CSS files can
be generated.

```shell
ddev gulp 
```

This can be run in any project folder. Keep it running in the background whilst 
making changes to scss files. Remember to turn off CSS and JS aggregation in 
Drupal and clear caches in both Drupal and the browser before viewing the 
changes.
