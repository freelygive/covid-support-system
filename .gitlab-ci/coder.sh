#!/usr/bin/env bash

# Get the version of coder from composer.lock.
CODER_VERSION=$(grep '"name": "drupal/coder"' -A 1 composer.lock | grep 'version' | grep -o '[0-9\.]*')

# Globally require it and the sniffer installer.
composer global require drupal/coder:$CODER_VERSION dealerdirect/phpcodesniffer-composer-installer

# Run our test on the modules and themes.
~/.composer/vendor/bin/phpcs
