# Set an alias for PHP Unit.
if [[ -f "$WEBSERVER_DOCROOT/vendor/bin/phpunit" ]]; then
  alias phpunit="$WEBSERVER_DOCROOT/vendor/bin/phpunit --bootstrap /var/www/html/web/core/tests/bootstrap.php"
fi

# Set the default phpcs alias and defaults.
if [[ -f "$WEBSERVER_DOCROOT/vendor/bin/phpcs" ]]; then
  alias phpcs="$WEBSERVER_DOCROOT/vendor/bin/phpcs"
  alias phpcs-d="$WEBSERVER_DOCROOT/vendor/bin/phpcs --standard=Drupal"
  $WEBSERVER_DOCROOT/vendor/bin/phpcs --config-set default_standard Drupal
  $WEBSERVER_DOCROOT/vendor/bin/phpcs --config-set show_progress 1
  $WEBSERVER_DOCROOT/vendor/bin/phpcs --config-set colors 1
fi
