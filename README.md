# COVID-19 Support System

### What is a Coronavirus Mutual Aid Support System?

This is a system that allows people to say they would like to volunteer to help
people, whilst others can request support from the volunteers. We then provide
tools for staff and admins to match and manage these people so that they can
receive the appropriate help. This will eventually extend to making it possible
for people to self-organise when we have improved safeguarding.

This is built on top of the popular content management system Drupal and a
module called the Contacts module Freely Give maintain that allows Drupal to be
used as a CRM system.

A video can be seen here: https://www.loom.com/share/28a35b0706924a3e96914ed18f6cd48d

### How to contribute
FreelyGive have made this repository public to aid quick sharing of the system
for others to use. Please feel free to make use of it and to contribute back
any improvements that you make. Please see [CONTRIBUTING.md](CONTRIBUTING.md)
for more information.

### Configuration management
Please see
[CONTRIBUTING.md - Configuration management](CONTRIBUTING.md#configuration-management)
for details on how to manage configuration for a single instance. This
currently involves a slightly more advanced knowledge of Drupal's configuration
system, but we are looking to simplify this in the future.
