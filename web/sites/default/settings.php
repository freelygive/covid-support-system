<?php

// @codingStandardsIgnoreFile

/**
 * @file
 * Generic settings for the COVID-19 Support System.
 */

/**
 * Location of the site configuration files.
 */
$config_directories = [
  CONFIG_SYNC_DIRECTORY => '../config/sync'
];

/**
 * Settings:
 *
 * $settings contains environment-specific configuration, such as the files
 * directory and reverse proxy address, and temporary configuration, such as
 * security overrides.
 *
 * @see \Drupal\Core\Site\Settings::get()
 */

/**
 * Salt for one-time login links, cancel links, form tokens, etc.
 *
 * This variable will be set to a random value by the installer. All one-time
 * login links will be invalidated if the value is changed. Note that if your
 * site is deployed on a cluster of web servers, you must ensure that this
 * variable has the same value on each server.
 *
 * For enhanced security, you may set this variable to the contents of a file
 * outside your document root; you should also ensure that this file is not
 * stored with backups of your database.
 *
 * Example:
 * @code
 *   $settings['hash_salt'] = file_get_contents('/home/example/salt.txt');
 * @endcode
 */
$settings['hash_salt'] = 'ZcRqfFIwxsrEO4Pg-FaYEjyPGwQm_I5txkDjfSodJELgZsCcsleg2ovWDzQ-6B4rAYxcbYDDfg';

/**
 * Authorized file system operations.
 */
$settings['allow_authorize_operations'] = FALSE;

/**
 * Public file path.
 */
$settings['file_public_path'] = 'sites/default/files';

/**
 * Private file path.
 */
$settings['file_private_path'] = '../../private';

/**
 * A custom theme for the offline page.
 */
$settings['maintenance_theme'] = 'covid_theme';

/**
 * Load services definition file.
 */
$settings['container_yamls'][] = $app_root . '/' . $site_path . '/services.yml';
$settings['container_yamls'][] = $app_root . '/' . $site_path . '/local.services.yml';

/**
 * The default list of directories that will be ignored by Drupal's file API.
 */
$settings['file_scan_ignore_directories'] = [
  'node_modules',
  'bower_components',
];

/**
 * The default number of entities to update in a batch process.
 *
 * This is used by update and post-update functions that need to go through and
 * change all the entities on a site, so it is useful to increase this number
 * if your hosting configuration (i.e. RAM allocation, CPU speed) allows for a
 * larger number of entities to be processed in a single batch run.
 */
$settings['entity_update_batch_size'] = 50;

/**
 * Entity update backup.
 */
$settings['entity_update_backup'] = TRUE;

/**
 * Load local override configuration, if available.
 */
if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
  include $app_root . '/' . $site_path . '/settings.local.php';
}

// Automatically generated include for settings managed by ddev.
if (file_exists($app_root . '/' . $site_path . '/settings.ddev.php')) {
  include $app_root . '/' . $site_path . '/settings.ddev.php';
  $settings['container_yamls'][] = $app_root . '/' . $site_path . '/ddev.services.yml';

  // Configure the SOLR connection for DDev installs.
  $config['search_api.server.solr_local']['backend_config']['connector_config']['host'] = 'covid.ddev.site';
  $config['search_api.server.solr_local']['backend_config']['connector_config']['core'] = 'dev';

  // Index directly as DDev doesn't have cron running.
  $config['search_api.index.contacts_index']['options']['index_directly'] = TRUE;

  // Set up the filesystem for DDev.
  $config['system.file']['path']['temporary'] = '/tmp';
  $settings['file_private_path'] = '../.ddev/private';

  // DDev is dev by default, with no instance code.
  if (!isset($config['config_split.config_split.dev']['status'])) {
    $config['config_split.config_split.dev']['status'] = TRUE;
  }
  if (!isset($config['config_split.config_split.instance']['status'])) {
    $config['config_split.config_split.instance']['status'] = FALSE;
  }
}

// Configure config split settings if not explicitly defined.
if (!isset($config['config_split.config_split.dev']['status'])) {
  $config['config_split.config_split.dev']['status'] = FALSE;
}
if (!isset($config['config_split.config_split.instance']['status'])) {
  $config['config_split.config_split.instance']['status'] = TRUE;
}
