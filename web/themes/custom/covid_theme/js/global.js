/**
 * @file
 * Global utilities.
 *
 */
(function($, Drupal) {

  'use strict';

  /**
   * Change link text when expanded/collapsed.
   */
  Drupal.behaviors.covid_theme_collapse_text = {
    attach: function(context, settings) {
      $('.collapse-text .text', context).click(function() {
        $(this, context).text(function(i,old) {
          return old == 'Expand' ?  'Collapse' : 'Expand';
        });
      });
    }
  };

})(jQuery, Drupal);
