<?php

namespace Drupal\covid_crm_tools\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Access\GroupAccessResult;
use Drupal\user\UserInterface;
use Symfony\Component\Routing\Route;

/**
 * Checks if passed parameter matches the route configuration.
 *
 * @DCG
 * To make use of this access checker add '_any_group_permission: Some value'
 *   entry to route definition under requirements section.
 */
class AnyGroupPermissionAccessChecker implements AccessInterface {

  /**
   * The user entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $userStorage;

  /**
   * Construct the access checker.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->userStorage = $entity_type_manager->getStorage('user');
  }

  /**
   * Checks access.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The parametrized route.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account to check access for.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    $permission = $route->getRequirement('_any_group_permission');

    // Don't interfere if no permission was specified.
    if ($permission === NULL) {
      return AccessResult::neutral();
    }

    // Check whether we should be using the user in the route for the group
    // permission check.
    if ($param = $route->getOption('_any_group_permissions_parameter')) {
      $user = $route_match->getParameter($param);
      $result = $this->checkPermission($permission, $user);
      if ($result instanceof RefinableCacheableDependencyInterface) {
        $result->addCacheableDependency($user);
      }
      return $result;
    }
    else {
      return $this->checkPermission($permission, $account);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return ['group_content_list'];
  }

  /**
   * Run a check for the given permissions in any group.
   *
   * @param string $permission
   *   A permission string. Multiple values can be separated with , or + for OR
   *   and AND respectively.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account to check.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function checkPermission(string $permission, AccountInterface $account): AccessResultInterface {
    // Don't interfere if the account is anonymous.
    if ($account->isAnonymous()) {
      return AccessResult::neutral()
        ->addCacheContexts(['user.roles:anonymous']);
    }

    // Ensure we have a full user.
    /** @var \Drupal\user\UserInterface $user */
    $user = $account instanceof UserInterface ?
      $account :
      $this->userStorage->load($account->id());

    $result = AccessResult::neutral('User has no groups.')
      ->addCacheContexts(['user'])
      ->addCacheableDependency($user);

    /** @var \Drupal\group\Entity\GroupContentInterface $group_content */
    foreach ($user->get('organisations')->referencedEntities() as $group_content) {
      $group = $group_content->getGroup();

      // Allow to conjunct the permissions with OR ('+') or AND (',').
      $split = explode(',', $permission);
      if (count($split) > 1) {
        $result = $result->orIf(GroupAccessResult::allowedIfHasGroupPermissions($group, $account, $split, 'AND'));
      }
      else {
        $split = explode('+', $permission);
        $result = $result->orIf(GroupAccessResult::allowedIfHasGroupPermissions($group, $account, $split, 'OR'));
      }
    }

    return $result;
  }

}
