<?php

namespace Drupal\covid_crm_tools\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\covid_crm_tools\Controller\UserDashboardController;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    foreach ($collection as $route) {
      if (substr($route->getPath(), 0, 12) == '/user/{user}') {
        $route->setDefault('_title_callback', UserDashboardController::class . '::title');
      }
    }
  }

}
