<?php

namespace Drupal\covid_crm_tools\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\covid_tweaks\Privacy;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller routines for user routes.
 */
class UserDashboardController extends ControllerBase {

  /**
   * The privacy helper.
   *
   * @var \Drupal\covid_tweaks\Privacy
   */
  protected $privacy;

  /**
   * Constructs a UserDashboardController object.
   *
   * @param \Drupal\covid_tweaks\Privacy $privacy
   *   The privacy helper.
   */
  public function __construct(Privacy $privacy) {
    $this->privacy = $privacy;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('covid_tweaks.privacy'),
    );
  }

  /**
   * Get the page title for the user dashboard pages.
   *
   * @param int|\Drupal\user\UserInterface $user
   *   The User entity.
   *
   * @return array
   *   The privacy aware page title.
   *
   * @see \Drupal\covid_crm_tools\Routing\RouteSubscriber::alterRoutes
   */
  public function title($user) {
    return $this->privacy->getDisplayName($user);
  }

}
