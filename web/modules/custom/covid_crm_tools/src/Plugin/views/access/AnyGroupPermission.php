<?php

namespace Drupal\covid_crm_tools\Plugin\views\access;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Plugin\views\access\GroupPermission;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * Views access plugin for viewing Group Members.
 *
 * @ViewsAccess(
 *   id = "covid_crm_tools_group_members",
 *   title = @Translation("Group permission (any group)"),
 *   help = @Translation("Access will be granted to users with the specified group permission string in any of their groups.")
 * )
 */
class AnyGroupPermission extends GroupPermission {

  /**
   * The any group access checker.
   *
   * @var \Drupal\covid_crm_tools\Access\AnyGroupPermissionAccessChecker
   */
  protected $accessChecker;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $plugin = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $plugin->accessChecker = $container->get('access_check.covid_crm_tools.any_group_permission');
    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account) {
    $user = \Drupal::routeMatch()->getParameter('user');
    if (!($user instanceof UserInterface)) {
      $user = User::load($user);
    }
    // Pass off the the access handler to avoid duplication.
    return $this->accessChecker
      ->checkPermission(implode('+', $this->options['group_permission']), $user)
      ->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  public function alterRouteDefinition(Route $route) {
    $route->setRequirement('_any_group_permission', implode('+', $this->options['group_permission']));
    $route->setOption('_any_group_permissions_parameter', 'user');
    $route->setRequirement('_entity_access', 'user.dashboard');

    // Upcast the user for entity access checks.
    $route->setOption('parameters', ['user' => ['type' => 'entity:user']]);
  }

  /**
   * {@inheritdoc}
   */
  public function summaryTitle() {
    $permissions = $this->permissionHandler->getPermissions(TRUE);

    $permission_labels = [];
    foreach ($this->options['group_permission'] as $permission) {
      if (isset($permissions[$permission])) {
        $permission_labels[] = strip_tags($permissions[$permission]['title']);
      }
      else {
        $permission_labels[] = $permission;
      }
    }

    return implode(', ', $permission_labels);
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['group_permission']['default'] = [];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return ['user'];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['group_permission']['#multiple'] = TRUE;
    $form['group_permission']['#description'] = $this->t('The user in the {user} parameter must have all of the selected permissions for any of their organisations.');
  }

}
