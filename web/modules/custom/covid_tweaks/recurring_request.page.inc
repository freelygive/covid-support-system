<?php

/**
 * @file
 * Contains recurring_request.page.inc.
 *
 * Page callback for Recurring request entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Recurring request templates.
 *
 * Default template: recurring_request.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_recurring_request(array &$variables) {
  // Fetch RecurringRequest Entity Object.
  $recurring_request = $variables['elements']['#recurring_request'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
