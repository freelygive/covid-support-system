<?php

namespace Drupal\covid_tweaks\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Recurring request entities.
 *
 * @ingroup covid_tweaks
 */
interface RecurringRequestInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Recurring request name.
   *
   * @return string
   *   Name of the Recurring request.
   */
  public function getName();

  /**
   * Sets the Recurring request name.
   *
   * @param string $name
   *   The Recurring request name.
   *
   * @return \Drupal\covid_tweaks\Entity\RecurringRequestInterface
   *   The called Recurring request entity.
   */
  public function setName($name);

  /**
   * Gets the Recurring request creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Recurring request.
   */
  public function getCreatedTime();

  /**
   * Sets the Recurring request creation timestamp.
   *
   * @param int $timestamp
   *   The Recurring request creation timestamp.
   *
   * @return \Drupal\covid_tweaks\Entity\RecurringRequestInterface
   *   The called Recurring request entity.
   */
  public function setCreatedTime($timestamp);

}
