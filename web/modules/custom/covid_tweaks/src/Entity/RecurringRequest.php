<?php

namespace Drupal\covid_tweaks\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\node\Entity\Node;
use Drupal\user\UserInterface;

/**
 * Defines the Recurring request entity.
 *
 * @ingroup covid_tweaks
 *
 * @ContentEntityType(
 *   id = "recurring_request",
 *   label = @Translation("Recurring request"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\covid_tweaks\RecurringRequestListBuilder",
 *     "views_data" = "Drupal\covid_tweaks\Entity\RecurringRequestViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\covid_tweaks\Form\RecurringRequestForm",
 *       "add" = "Drupal\covid_tweaks\Form\RecurringRequestForm",
 *       "edit" = "Drupal\covid_tweaks\Form\RecurringRequestForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\covid_tweaks\RecurringRequestAccessControlHandler",
 *   },
 *   base_table = "recurring_request",
 *   translatable = FALSE,
 *   admin_permission = "administer recurring request entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *   },
 *   links = {
 *     "canonical" = "/recurring_request/{recurring_request}",
 *     "add-form" = "/recurring_request/add",
 *     "edit-form" = "/recurring_request/{recurring_request}/edit",
 *     "delete-form" = "/recurring_request/{recurring_request}/delete",
 *     "collection" = "/admin/structure/recurring_request",
 *   },
 *   field_ui_base_route = "entity.recurring_request.collection"
 * )
 */
class RecurringRequest extends ContentEntityBase implements RecurringRequestInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('title', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Requested By'))
      ->setDescription(t('The user ID the person making the request'))
      ->setRevisionable(TRUE)
      ->setRequired(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The title of the request'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    // Uses text_with_summary to match the body field on requests, even though
    // we don't actually use the summary.
    $fields['body'] = BaseFieldDefinition::create('text_with_summary')
      ->setLabel(t('Description'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['recurrence'] = BaseFieldDefinition::create('date_recur')
      ->setLabel(t('Recurrence'))
      ->setSetting('precreate', 'P1Y')
      ->setSetting('parts', [
        'all' => FALSE,
        'frequencies' => [
          'SECONDLY' => [],
          'MINUTELY' => [],
          'HOURLY' => ['*'],
          'DAILY' => ['*'],
          'WEEKLY' => ['*'],
          'MONTHLY' => ['*'],
          'YEARLY' => [],
        ],
      ])
      ->setDefaultValue([
        [
          'default_date_type' => 'now',
          'default_date' => 'now',
          'default_end_date_type' => '',
          'default_end_date' => '',
          'default_date_time_zone' => 'Europe/London',
          'default_time_zone' => 'Europe/London',
          'default_rrule' => '',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['tags'] = BaseFieldDefinition::create('entity_reference')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setLabel(t('Tags'))
      ->setSettings([
        'target_type' => 'taxonomy_term',
        'handler' => 'default:taxonomy_term',
        'handler_settings' => [
          'target_bundles' => ['tags' => 'tags'],
          'sort' => [
            'field' => 'name',
            'direction' => 'asc',
          ],
          'auto_create' => FALSE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['tags_other'] = BaseFieldDefinition::create('entity_reference')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setLabel(t('Tags (other)'))
      ->setSettings([
        'target_type' => 'taxonomy_term',
        'handler' => 'default:taxonomy_term',
        'handler_settings' => [
          'target_bundles' => ['other_tags' => 'other_tags'],
          'sort' => [
            'field' => 'name',
            'direction' => 'asc',
          ],
          'auto_create' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['last_generated_occurrence'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Last generated occurrence'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * Generates the next request.
   *
   * @return \Drupal\node\Entity\Node
   *   The request entity.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function createNextRequestFromSchedule() : Node {
    if ($max_occurrence_generated = $this->get('last_generated_occurrence')->value) {
      if ($max_occurrence_generated == -1) {
        // The schedule has finished.
        return NULL;
      }
      $max_occurrence_generated = (new \DateTime())
        ->setTimezone(new \DateTimeZone('Europe/London'))
        ->setTimestamp($max_occurrence_generated)
        ->modify('+1 day');
    }
    else {
      $max_occurrence_generated = (new \DateTime())
        ->setTimezone(new \DateTimeZone('Europe/London'))
        ->setTime(0, 0, 0);
    }

    /* @var \Drupal\date_recur\Plugin\Field\FieldType\DateRecurItem $item */
    $item = $this->get('recurrence')->first();
    $h = $item->getHelper();
    $next = $h->getOccurrences($max_occurrence_generated, NULL, 1);

    if (count($next)) {
      $next = reset($next);
      $this->set('last_generated_occurrence', $next->getStart()->getTimestamp());

      $date = clone $next->getStart();
      $date = $date->format(DateTimeItemInterface::DATE_STORAGE_FORMAT);

      // Generate an request.
      /* @var \Drupal\node\Entity\Node $request*/
      $request = Node::create(['type' => 'request']);
      $request->set('title', $this->get('title')->value);
      $request->set('field_tags', $this->get('tags')->getValue());
      $request->set('field_tags_other', $this->get('tags_other')->getValue());
      $request->set('field_date', $date);
      $request->set('body', $this->get('body')->getValue());
      $request->set('field_associated_recurrence', $this);
      $request->setOwnerId($this->getOwnerId());
      return $request;
    }
    else {
      // There aren't any - the schedule has finished.
      // Set to -1 so it doesn't try and generate again.
      $this->set('field_last_generated_occurrence', -1);
    }

    return NULL;
  }

}
