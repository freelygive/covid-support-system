<?php

namespace Drupal\covid_tweaks;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Recurring request entity.
 *
 * @see \Drupal\covid_tweaks\Entity\RecurringRequest.
 */
class RecurringRequestAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\covid_tweaks\Entity\RecurringRequestInterface $entity */

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view recurring request entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit recurring request entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete recurring request entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add recurring request entities');
  }

}
