<?php

namespace Drupal\covid_tweaks\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\covid_tweaks\Controller\ProfileController;
use Drupal\covid_tweaks\Controller\RequestController;
use Drupal\covid_tweaks\Form\CovidAddIndivForm;
use Symfony\Component\Routing\RouteCollection;

/**
 * Modify routes for the Covid system.
 */
class CovidRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('node.add')) {
      $route->setDefault('_controller', RequestController::class . '::add');
      $options = $route->getOptions();
      // Remove the _node_operation_route option, forces use
      // of the admin theme.
      unset($options['_node_operation_route']);
      $route->setOptions($options);
    }

    if ($route = $collection->get('contacts.add_indiv_form')) {
      $route->setDefault('_form', CovidAddIndivForm::class);
    }

    if ($route = $collection->get('view.connections.page_1')) {
      $route->setRequirement('_entity_access', 'user.dashboard');
      $route->setOption('parameters', ['user' => ['type' => 'entity:user']]);
    }

    if ($route = $collection->get('profile.user_page.single')) {
      $route->setDefault('_controller', ProfileController::class . '::singlePage');
    }
  }

}
