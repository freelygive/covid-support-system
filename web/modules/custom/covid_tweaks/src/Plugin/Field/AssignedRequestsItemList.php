<?php

namespace Drupal\covid_tweaks\Plugin\Field;

/**
 * Computed item list for the Requests assigned to a User.
 *
 * @package Drupal\contacts_events\Plugin\Field
 */
class AssignedRequestsItemList extends UserRequestsItemList {

  /**
   * {@inheritdoc}
   */
  protected function getQuery() {
    $entity = $this->getEntity();
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'request')
      ->condition('field_assignees', $entity->id())
      ->condition('field_status', 'assigned')
      ->condition('status', TRUE);

    return $query;
  }

}
