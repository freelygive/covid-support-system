<?php

namespace Drupal\covid_tweaks\Plugin\Field\FieldFormatter;

use Drupal\Core\Datetime\DateHelper;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\date_recur\Plugin\Field\FieldType\DateRecurItem;
use Drupal\date_recur_modular\DateRecurModularUtilityTrait;

/**
 * Basic recurring date formatter.
 *
 * @FieldFormatter(
 *   id = "covid_recur",
 *   label = @Translation("Date recur Covid formatter"),
 *   field_types = {
 *     "date_recur"
 *   }
 * )
 */
class CovidDateRecurrenceFormatter extends FormatterBase {

  use DateRecurModularUtilityTrait;

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $elements[$delta] = $this->viewItem($item);
    }

    return $elements;
  }

  /**
   * Builds the output.
   *
   * @param \Drupal\date_recur\Plugin\Field\FieldType\DateRecurItem $item
   *   Item.
   *
   * @return array
   *   Render array.
   *
   * @throws \Exception
   */
  private function viewItem(DateRecurItem $item) {
    $rule = $this->getRule($item);

    $weekdaysKeys = ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA'];
    $weekdayLabels = DateHelper::weekDaysAbbr(TRUE);
    $weekdays = array_combine($weekdaysKeys, $weekdayLabels);

    $parts = $rule->getParts();
    $days = array_map(function ($d) use ($weekdays) {
      return (string) $weekdays[$d];
    }, explode(',', $parts['BYDAY']));
    $days = implode(', ', $days);

    $date = (new \DateTime($item->value))
      ->setTimezone(new \DateTimeZone('Europe/London'))
      ->format('d/m/Y');

    $content = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => [
        $this->t('Recurs: @type', ['@type' => ucwords(strtolower($parts['FREQ']))]),
        $this->t('Starting on: @date', ['@date' => $date]),
        $this->t('Days: @days', ['@days' => $days]),
      ],
      '#wrapper_attributes' => ['class' => 'container'],
    ];

    return $content;
  }

}
