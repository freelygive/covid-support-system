<?php

namespace Drupal\covid_tweaks\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\user\Plugin\Field\FieldFormatter\UserNameFormatter;

/**
 * Username formatter for the COVID system.
 */
class CovidUserNameFormatter extends UserNameFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    // The core handler only works if there is a name set as it loops over the
    // items. That doesn't work for decoupled users, so pull the user directly
    // from the items and assume only ever one delta.
    // @see \Drupal\decoupled_auth\Plugin\Field\FieldFormatter\DecoupledUserNameFormatter
    /** @var $user \Drupal\user\UserInterface */
    if ($user = $items->getEntity()) {
      if ($this->getSetting('link_to_entity')) {
        $elements[0] = [
          '#theme' => 'username',
          '#account' => $user,
          '#link_options' => ['attributes' => ['rel' => 'user']],
          '#cache' => [
            'tags' => $user->getCacheTags(),
          ],
        ];
      }
      // Core also bypasses the theme for non links, but we need it for privacy.
      else {
        $elements[0] = [
          '#theme' => 'username',
          '#account' => $user,
          '#cache' => [
            'tags' => $user->getCacheTags(),
          ],
        ];
      }
    }

    return $elements;
  }

}
