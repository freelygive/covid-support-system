<?php

namespace Drupal\covid_tweaks\Plugin\Field;

/**
 * Computed item list for the Requests assigned to a User.
 *
 * @package Drupal\contacts_events\Plugin\Field
 */
class UnassignedRequestsItemList extends UserRequestsItemList {

  /**
   * {@inheritdoc}
   */
  protected function getQuery() {
    $query = parent::getQuery();
    $query->condition('field_status', 'requested');
    return $query;
  }

}
