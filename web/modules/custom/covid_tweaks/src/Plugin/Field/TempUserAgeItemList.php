<?php

namespace Drupal\covid_tweaks\Plugin\Field;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Computed item list for the label of an entity.
 *
 * @package Drupal\contacts_events\Plugin\Field
 */
class TempUserAgeItemList extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    $this->list = [];

    $map = [
      0 => 'Under 20',
      10 => '10 - 20',
      20 => '20 - 30',
      30 => '30 - 40',
      40 => '40 - 50',
      50 => '50 - 60',
      60 => '60+',
    ];

    $entity = $this->getEntity();
    if (!$entity->isNew()) {
      $age = 'Not specified';

      if (!$entity->get('profile_crm_indiv')->isEmpty()) {
        $dob = $entity->get('profile_crm_indiv')->entity->get('crm_dob')->date;
        if ($dob instanceof DrupalDateTime) {
          $today = new DrupalDateTime();
          $interval = $today->diff($dob);
          $age = $interval->y;

          $age = floor($age / 10) * 10;

          if ($age >= 60) {
            $age = 60;
          }

          $age = $map[$age];
        }
      }

      $this->list[0] = $this->createItem(0, $age);
    }
  }

}
