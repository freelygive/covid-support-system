<?php

namespace Drupal\covid_tweaks\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'covid_tweaks_pending_volunteer' field type.
 *
 * @property string target_id
 * @property \Drupal\user\Entity\User|null entity
 * @property string message
 *
 * @FieldType(
 *   id = "covid_tweaks_pending_volunteer",
 *   label = @Translation("Pending Volunteer"),
 *   category = @Translation("Reference"),
 *   default_widget = "entity_reference_autocomplete",
 *   default_formatter = "entity_reference_label",
 *   list_class = "\Drupal\Core\Field\EntityReferenceFieldItemList",
 *   no_ui = true
 * )
 */
class PendingVolunteerItem extends EntityReferenceItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'target_type' => 'user',
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);

    $properties['message'] = DataDefinition::create('string')
      ->setLabel(t('Message'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);

    $schema['columns']['message'] = [
      'description' => 'Message provided by volunteer.',
      'type' => 'varchar',
      'length' => 255,
    ];

    return $schema;
  }

}
