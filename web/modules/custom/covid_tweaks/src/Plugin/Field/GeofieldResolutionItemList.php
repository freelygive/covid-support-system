<?php

namespace Drupal\covid_tweaks\Plugin\Field;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\geofield\WktGeneratorInterface;

/**
 * Computed item list for Geofield that can have a specified resolution.
 */
class GeofieldResolutionItemList extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * The WKT generator.
   *
   * @var \Drupal\geofield\WktGeneratorInterface
   */
  protected static $wktGenerator;

  /**
   * The precision to use or FALSE to not round.
   *
   * @var int|false
   */
  protected static $precision;

  /**
   * The divider to use or NULL to not use a divider.
   *
   * @var int|null
   */
  protected static $divider;

  /**
   * Get the WKT generator.
   *
   * @return \Drupal\geofield\WktGeneratorInterface
   *   The WKT generator.
   */
  protected static function getWktGenerator(): WktGeneratorInterface {
    if (!isset(self::$wktGenerator)) {
      self::$wktGenerator = \Drupal::service('geofield.wkt_generator');
    }
    return self::$wktGenerator;
  }

  /**
   * Get the location privacy settings from config.
   */
  protected static function initSettings(): void {
    if (!isset(self::$precision)) {
      $config = \Drupal::config('covid_tweaks.settings');
      self::$precision = $config->get('privacy.location.precision') ?? FALSE;
      if (self::$precision) {
        self::$divider = $config->get('privacy.location.divider');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    $source = $this->definition->getSetting('source');

    $items = $this->getEntity()->get($source);
    $i = 0;
    foreach ($items as $item) {
      // We don't support anything other than points.
      if ($item->geo_type !== 'Point') {
        throw new \InvalidArgumentException("Unable to handle geometry type {$item->geo_type}.");
      }

      // Generate the rounded WKT.
      $wkt = self::getWktGenerator()->wktBuildPoint([
        $this->roundNumber($item->lon),
        $this->roundNumber($item->lat),
      ]);

      // Add the item.
      $this->list[$i] = $this->createItem($i, $wkt);
      $i++;
    }
  }

  /**
   * Round the number based on the privacy settings.
   *
   * @param float $number
   *   The number to round.
   *
   * @return float
   *   The rounded number.
   */
  protected function roundNumber(float $number): float {
    self::initSettings();

    // Return the number as is if no rounding is set.
    if (self::$precision === FALSE) {
      return $number;
    }

    // Apply simple rounding to start.
    $number = round($number, self::$precision);

    // If using a divider, multiply by 10^precision, get the modulo, adjust the
    // number accordingly and then divide back by 10^precision.
    if (self::$divider) {
      $multiplier = pow(10, self::$precision);
      $number = $number * $multiplier;
      $modulo = $number % self::$divider;
      $adjustment = abs($modulo) >= self::$divider / 2 ? $modulo : -$modulo;
      $number = ($number + $adjustment) / $multiplier;
    }

    return $number;
  }

}
