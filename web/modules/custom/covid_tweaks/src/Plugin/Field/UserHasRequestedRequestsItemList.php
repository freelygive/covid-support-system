<?php

namespace Drupal\covid_tweaks\Plugin\Field;

/**
 * Computed item list for the label of an entity.
 *
 * @package Drupal\contacts_events\Plugin\Field
 */
class UserHasRequestedRequestsItemList extends UserHasRequestsItemList {

  /**
   * {@inheritdoc}
   */
  protected function getQuery() {
    $query = parent::getQuery();
    $query->condition('field_status', ['requested'], 'IN');
    return $query;
  }

}
