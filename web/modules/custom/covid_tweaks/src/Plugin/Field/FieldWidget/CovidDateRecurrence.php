<?php

namespace Drupal\covid_tweaks\Plugin\Field\FieldWidget;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\date_recur_modular\Plugin\Field\FieldWidget\DateRecurModularAlphaWidget;

/**
 * Recurrence widget.
 *
 * @FieldWidget(
 *   id = "date_recur_covid",
 *   label = @Translation("Modular: Covid Custom"),
 *   field_types = {
 *     "date_recur"
 *   }
 * )
 */
class CovidDateRecurrence extends DateRecurModularAlphaWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    // Hide timezone.
    $element['time_zone']['#access'] = FALSE;
    // Switch start field from date + time to just
    // date.
    $element['start']['#type'] = 'date';
    // Also need to ensure time portion is removed.
    if ($element['start']['#default_value'] instanceof DrupalDateTime) {
      $element['start']['#default_value'] = $element['start']['#default_value']->format('Y-m-d');
    }

    // Hide end date.
    $element['end']['#access'] = FALSE;
    $element['start']['#title_display'] = 'before';
    $element['ends_date']['ends_date']['#type'] = 'date';

    // Remove modes we don't need.
    unset($element['mode']['#options']['once']);
    unset($element['mode']['#options']['multiday']);

    // Move sunday to the end, so we're Monday -> Sunday, rather than
    // Sunday -> saturday.
    $sunday = $element['weekdays']['#options']['SU'];
    unset($element['weekdays']['#options']['SU']);
    $element['weekdays']['#options']['SU'] = $sunday;

    // Replace the element validator with one that actually works for us.
    $element['#element_validate'] = [[static::class, 'doValidate']];

    return $element;
  }

  /**
   * Validate.
   *
   * @param array $element
   *   Element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   * @param array $complete_form
   *   Form.
   */
  public static function doValidate(array &$element, FormStateInterface $form_state, array &$complete_form) {
    $start = $form_state->getValue(array_merge($element['#parents'], ['start']));

    // Convert to a DrupalDateTime instance as the base class needs this.
    if (!empty($start)) {
      // Make sure both are the same.
      $start = new DrupalDateTime($start);
      $end = new DrupalDateTime($start);
      $form_state->setValue(array_merge($element['#parents'], ['start']), $start);
      $form_state->setValue(array_merge($element['#parents'], ['end']), $end);
    }

    DateRecurModularAlphaWidget::validateModularWidget($element, $form_state, $complete_form);
  }

}
