<?php

namespace Drupal\covid_tweaks\Plugin\Field;

use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Computed item list for a Users requests.
 *
 * @package Drupal\contacts_events\Plugin\Field
 */
class UserRequestsItemList extends EntityReferenceFieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    $this->list = [];

    $entity = $this->getEntity();
    if (!$entity->isNew()) {
      $query = $this->getQuery();
      $results = $query->execute();

      $i = 0;
      foreach ($results as $result) {
        $this->list[$i] = $this->createItem($i, $result);
        $i++;
      }
    }
  }

  /**
   * Get the query to check results for.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   The query to be checked.
   */
  protected function getQuery() {
    $entity = $this->getEntity();
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'request')
      ->condition('uid', $entity->id())
      ->condition('status', TRUE);

    return $query;
  }

}
