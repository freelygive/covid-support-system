<?php

namespace Drupal\covid_tweaks\Plugin\Field;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Computed item list for the label of an entity.
 *
 * @package Drupal\contacts_events\Plugin\Field
 */
class UserHasRequestsItemList extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    $this->list = [];

    $entity = $this->getEntity();
    if (!$entity->isNew()) {
      $query = $this->getQuery();
      $results = $query->execute();
      $this->list[0] = $this->createItem(0, (bool) !empty($results));
    }
  }

  /**
   * Get the query to check results for.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   The query to be checked.
   */
  protected function getQuery() {
    $entity = $this->getEntity();
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'request')
      ->condition('uid', $entity->id())
      ->condition('status', TRUE);

    return $query;
  }

}
