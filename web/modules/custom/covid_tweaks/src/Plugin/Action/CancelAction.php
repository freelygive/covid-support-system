<?php

namespace Drupal\covid_tweaks\Plugin\Action;

use Drupal\Core\Session\AccountInterface;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;

/**
 * Publishes an entity.
 *
 * @Action(
 *   id = "covid_tweaks_request_cancel",
 *   action_label = @Translation("Cancel requests"),
 * )
 */
class CancelAction extends ViewsBulkOperationsActionBase {

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    /** @var \Drupal\node\NodeInterface $entity */
    $entity
      ->set('field_status', 'cancelled')
      ->setUnpublished()
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    /** @var \Drupal\Core\Entity\EntityInterface $object */
    return $object->access('cancel', $account, $return_as_object);
  }

}
