<?php

namespace Drupal\covid_tweaks\Plugin\Action;

use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\node\NodeInterface;
use Drupal\user\UserInterface;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Assigns a user to a request.
 *
 * @Action(
 *   id = "covid_assign_action",
 *   label = @Translation("Assign the user to a request"),
 *   type = "user",
 * )
 */
class AssignAction extends ViewsBulkOperationsActionBase implements ContainerFactoryPluginInterface, PluginFormInterface {

  use LoggerChannelTrait;

  /**
   * Route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $static = new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $static->routeMatch = $container->get('current_route_match');
    return $static;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(UserInterface $account = NULL, $data = NULL) {
    $request = $this->context['request'];
    if (!$request instanceof NodeInterface || $request->bundle() !== 'request') {
      $this->messenger()->addError($this->t('Only Requests can be assigned to someone.'));
      return;
    }

    $assignees = $request->get('field_assignees')->getValue();
    foreach ($assignees as $index => $assignee) {
      if ($assignee['target_id'] == $account->id()) {
        $this->messenger()->addError($this->t('User %user is already assigned to this request.', [
          '%user' => $account->label() . ' [' . $account->id() . ']',
        ]));
        return;
      }
    }

    $assignees[] = [
      'target_id' => $account->id(),
    ];
    $request->set('field_assignees', $assignees);
    $request->set('field_status', 'assigned');

    $this->messenger()->addStatus($this->t('Request %request has been assigned to %user.', [
      '%request' => $request->label() . ' [' . $request->id() . ']',
      '%user' => $account->label() . ' [' . $account->id() . ']',
    ]));
    $this->getLogger('covid_tweaks')->notice('Request %request has been assigned to %user.', [
      '%request' => $request->label() . ' [' . $request->id() . ']',
      '%user' => $account->label() . ' [' . $account->id() . ']',
    ]);

    $request->save();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    /** @var \Drupal\user\UserInterface $object */
    return $object->access('assign', $account, $return_as_object);
  }

}
