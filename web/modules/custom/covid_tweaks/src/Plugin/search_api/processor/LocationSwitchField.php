<?php

namespace Drupal\covid_tweaks\Plugin\search_api\processor;

use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Processor\ProcessorProperty;

/**
 * Adds customized location field that switches between others based on group.
 *
 * @see \Drupal\covid_tweaks\Plugin\search_api\processor\Property\AggregatedFieldProperty
 *
 * @SearchApiProcessor(
 *   id = "location_switch",
 *   label = @Translation("Location switch"),
 *   description = @Translation("Add customized location field to the index."),
 *   stages = {
 *     "add_properties" = 20,
 *     "preprocess_index" = 20,
 *   },
 *   locked = true,
 *   hidden = false,
 * )
 */
class LocationSwitchField extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];

    if ($datasource && $datasource->getEntityTypeId() == 'user') {
      $definition = [
        'label' => $this->t('Location switch field'),
        'description' => $this->t('A switch between other location fields.'),
        'type' => 'location',
        'processor_id' => $this->getPluginId(),
      ];
      $properties['location_switch'] = new ProcessorProperty($definition);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   *
   * Add a location_switch field and default to the geolocation field.
   */
  public function addFieldValues(ItemInterface $item) {
    $fields = $item->getFields(FALSE);
    $filtered_fields = $this->getFieldsHelper()
      ->filterForPropertyPath($fields, 'entity:user', 'location_switch');

    $required_properties_by_datasource = [
      NULL => [],
      $item->getDatasourceId() => [],
    ];
    $required_properties_by_datasource[$item->getDatasourceId()]['geolocation'] = 'geolocation';

    $property_values = $this->getFieldsHelper()
      ->extractItemValues([$item], $required_properties_by_datasource)[0];

    foreach ($filtered_fields as $field) {
      $field->addValue($property_values['geolocation']);
    }
  }

  /**
   * {@inheritdoc}
   *
   * Update the location_switch value using the location field from the users'
   * group if possible. Otherwise default to users' location.
   */
  public function preprocessIndexItems(array $items) {
    /** @var \Drupal\search_api\Item\Item $item */
    foreach ($items as &$item) {
      if (!$item->getField('location_switch')) {
        continue;
      }
      if (!$item->getOriginalObject()->getEntity()->get('organisations')->isEmpty()) {
        $latlon = $item->getField('latlon_org')->getValues();
      }
      else {
        $latlon = $item->getField('latlon')->getValues();
      }
      $field = $item->getField('location_switch');
      $field->setValues($latlon);
      $item->setField('location_switch', $field);
    }
  }

}
