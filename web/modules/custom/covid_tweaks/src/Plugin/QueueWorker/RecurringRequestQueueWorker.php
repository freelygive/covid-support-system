<?php

namespace Drupal\covid_tweaks\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\covid_tweaks\Entity\RecurringRequest;

/**
 * Queue worker for building requests from schedules.
 *
 * @QueueWorker(
 *   id = "covid_tweaks_recurring_schedule",
 *   title = @Translation("Builds requests from recurring schedules"),
 *   cron = {"time" = 90}
 * )
 */
class RecurringRequestQueueWorker extends QueueWorkerBase {

  const STATE_KEY = 'covid_tweaks.last_recurring_request_run';

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $batch_size = $data['batch_size'];
    $last_id_processed = $data['last_id_processed'];

    $query = \Drupal::entityQuery('recurring_request');
    $query->sort('id', 'ASC');
    $query->range(0, $batch_size);
    $query->condition('id', $last_id_processed, '>');

    // Find schedules where the last generated request has either never been
    // generated, or the last one generated is within the next 48 hours.
    $or = $query->orConditionGroup();
    $now = new \DateTime();
    $now->modify('+2 days');
    $or->condition('last_generated_occurrence', $now->getTimestamp(), '<=');
    $or->notExists('last_generated_occurrence');
    $query->condition($or);

    // -1 means the request has finished. Exclude these.
    $query->condition('last_generated_occurrence', -1, '<>');

    /* @var \Drupal\covid_tweaks\Entity\RecurringRequest[] $results */
    $results = RecurringRequest::loadMultiple($query->execute());

    foreach ($results as $recurring_request) {
      if ($request = $recurring_request->createNextRequestFromSchedule()) {
        $request->save();
      }
      $recurring_request->save();

      $last_id_processed = $request->id();
    }

    // If we have processed anything re-queue to ensure nothing
    // gets missed.
    if (!empty($results) && $last_id_processed > 0) {
      // Queue up the next batch.
      self::populateQueueImmediately($last_id_processed, $batch_size);
    }
  }

  /**
   * Populates the queue. Ensures it's run once a day.
   */
  public static function populateQueueOncePerDay() {
    $state = \Drupal::state();

    $request_time = new \DateTime();
    $request_time->setTimestamp(\Drupal::time()->getRequestTime());
    if ($last_run_time = $state->get(self::STATE_KEY, 0)) {
      $last_run = new \DateTime();
      $last_run->setTimestamp($last_run_time);
    }
    else {
      // If it's never been run, just set it to yesterday.
      $last_run = new \DateTime();
      $last_run->modify('-1 day')
        ->setTime(0, 0, 0);
    }

    if ($request_time->format('Y-M-d') != $last_run->format('Y-M-d')) {
      self::populateQueueImmediately();
      $state->set(self::STATE_KEY, $request_time->setTime(0, 0, 0)
        ->getTimestamp());
    }
  }

  /**
   * Populates the queue.
   *
   * @param int $last_id_processed
   *   Last ID processed.
   * @param int $batch_size
   *   Batch size.
   */
  protected static function populateQueueImmediately($last_id_processed = 0, $batch_size = 100) {
    $queue = \Drupal::queue('covid_tweaks_recurring_schedule');
    $queue->createItem([
      'last_id_processed' => $last_id_processed,
      'batch_size' => $batch_size,
    ]);
  }

}
