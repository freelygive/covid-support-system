<?php

namespace Drupal\covid_tweaks\Plugin\views\filter;

use Drupal\Component\Datetime\DateTimePlus;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\views\Plugin\views\filter\Date;

/**
 * The date popup views filter plugin.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("covid_date_popup")
 */
class DatePopup extends Date {

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    parent::valueForm($form, $form_state);

    // We don't support exposed offsets.
    if ($this->isExposed()) {
      if (isset($form['value']['type']['#options']['offset'])) {
        unset($form['value']['type']['#options']['offset']);
      }
      if (isset($form['value']['type']['#options']['date'])) {
        $form['value']['type']['#options']['date'] = $this->t('A date popup (provide defaults in format YYYY-MM-DD)');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildExposedForm(&$form, FormStateInterface $form_state) {
    parent::buildExposedForm($form, $form_state);

    if (!empty($this->options['expose']['identifier'])) {
      $identifier = $this->options['expose']['identifier'];
      // Identify wrapper.
      $wrapper_key = $identifier . '_wrapper';
      if (isset($form[$wrapper_key])) {
        $element = &$form[$wrapper_key][$identifier];
      }
      else {
        $element = &$form[$identifier];
      }

      // Detect filters that are using min/max.
      if (isset($element['min'])) {
        $element['min']['#type'] = 'date';
        $element['max']['#type'] = 'date';
        if (isset($element['value'])) {
          $element['value']['#type'] = 'date';
        }
      }
      else {
        $element['#type'] = 'date';
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function opBetween($field) {
    $operator_settings = $this->operators()[$this->operator];

    $format = ($this->definition['format'] ?? 'datetime') == 'datetime' ? DateTimeItemInterface::DATETIME_STORAGE_FORMAT : DateTimeItemInterface::DATE_STORAGE_FORMAT;

    // If the settings have 2 values, we can just pull the min/max and set the
    // times.
    if ($operator_settings['values'] == 2) {
      $a = (new DateTimePlus($this->value['min']))
        ->format($format);
      $b = (new DateTimePlus($this->value['max']))
        ->setTime(23, 59, 59)
        ->format($format);
    }
    // Otherwise we pull from value and split to set the min/max times.
    else {
      $a = (new DateTimePlus($this->value['value']))
        ->format($format);
      $b = (new DateTimePlus($this->value['value']))
        ->setTime(23, 59, 59)
        ->format($format);
    }

    // This is safe because we are manually scrubbing the values.
    // It is necessary to do it this way because $a and $b are formulas when
    // using an offset.
    // Pull the real operator or use the key if not set.
    $operator = strtoupper($operator_settings['real_operator'] ?? $this->operator);
    $this->query->addWhereExpression($this->options['group'], "$field $operator '$a' AND '$b'");
  }

  /**
   * {@inheritdoc}
   */
  protected function opSimple($field) {
    $datetime = new DateTimePlus($this->value['value']);
    $operator_settings = $this->operators()[$this->operator];

    if (isset($operator_settings['time'])) {
      switch ($operator_settings['time']) {
        case 'start':
          $datetime
            ->setTime(0, 0, 0);
          break;

        case 'end':
          $datetime
            ->setTime(23, 59, 59);
          break;
      }
    }

    $format = ($this->definition['format'] ?? 'datetime') == 'datetime' ? DateTimeItemInterface::DATETIME_STORAGE_FORMAT : DateTimeItemInterface::DATE_STORAGE_FORMAT;
    $value = $datetime->format($format);

    // This is safe because we are manually scrubbing the value.
    // It is necessary to do it this way because $value is a formula when
    // using an offset.
    $this->query->addWhereExpression($this->options['group'], "$field $this->operator '$value'");
  }

  /**
   * {@inheritdoc}
   */
  public function operators() {
    $operators = parent::operators();

    $format = $this->definition['format'] ?? 'datetime';
    if ($format == 'datetime') {
      $operators['<']['time'] = 'start';
      $operators['<=']['time'] = 'end';
      $operators['=']['method'] = 'opBetween';
      $operators['=']['real_operator'] = 'between';
      $operators['!=']['method'] = 'opBetween';
      $operators['!=']['real_operator'] = 'not between';
      $operators['>']['time'] = 'end';
      $operators['>=']['time'] = 'start';
    }

    return $operators;
  }

}
