<?php

namespace Drupal\covid_tweaks\Plugin\views\field;

use Drupal\Core\Url;
use Drupal\views\Plugin\views\field\LinkBase;
use Drupal\views\ResultRow;

/**
 * Field handler to present a link to the request review step.
 *
 * @ViewsField("covid_tweaks_request_review")
 */
class ReviewLink extends LinkBase {

  /**
   * Called to add the field to a query.
   */
  public function query() {
    $this->ensureMyTable();
    // Add the field.
    $params = $this->options['group_type'] != 'group' ? ['function' => $this->options['group_type']] : [];
    $this->field_alias = $this->query->addField($this->tableAlias, $this->realField, NULL, $params);

    $this->addAdditionalFields();
  }

  /**
   * {@inheritdoc}
   */
  protected function getUrlInfo(ResultRow $row) {
    $node = $this->getEntity($row);
    $target = $this->getValue($row);

    $params = [
      'node' => $node->id(),
      'user' => $target,
    ];
    return Url::fromRoute('covid_tweaks.request.review', $params);
  }

}
