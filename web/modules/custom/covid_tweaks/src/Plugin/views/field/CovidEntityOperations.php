<?php

namespace Drupal\covid_tweaks\Plugin\views\field;

use Drupal\views\Plugin\views\field\EntityOperations;
use Drupal\views\ResultRow;

/**
 * Renders all operations links for an entity.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("covid_tweaks_entity_operations")
 */
class CovidEntityOperations extends EntityOperations {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $build = parent::render($values);

    unset($build['#type']);
    $build['#theme'] = 'links';

    return $build;
  }

}
