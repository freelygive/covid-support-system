<?php

namespace Drupal\covid_tweaks\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views_bulk_operations\Plugin\views\field\ViewsBulkOperationsBulkForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the Views Bulk Operations field plugin.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("request_views_bulk_operations_bulk_form")
 */
class RequestViewsBulkOperationsBulkForm extends ViewsBulkOperationsBulkForm {

  /**
   * Route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $static = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $static->routeMatch = $container->get('current_route_match');
    return $static;
  }

  /**
   * {@inheritdoc}
   */
  public function viewsFormSubmit(array &$form, FormStateInterface $form_state) {
    $this->tempStoreData['request'] = $this->routeMatch->getParameter('node');
    parent::viewsFormSubmit($form, $form_state);
  }

}
