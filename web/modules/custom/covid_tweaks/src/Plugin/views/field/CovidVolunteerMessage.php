<?php

namespace Drupal\covid_tweaks\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Renders the message by the volunteer.
 *
 * @ViewsField("covid_tweaks_volunteer_message")
 */
class CovidVolunteerMessage extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $node = $this->getEntity($values);
    $target = $this->getValue($values);
    $message = '';

    foreach ($node->get('volunteers') as $item) {
      if ($item->target_id == $target) {
        $message = $item->message;
      }
    }

    $build = [
      '#markup' => $message,
    ];
    return $build;
  }

}
