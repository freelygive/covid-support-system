<?php

namespace Drupal\covid_tweaks\Plugin\Block;

use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an add request (frontend) block.
 *
 * @Block(
 *   id = "covid_tweaks_add_request_frontend",
 *   admin_label = @Translation("Add Request (Frontend)"),
 *   category = @Translation("Covid")
 * )
 */
class AddRequestFrontendBlock extends AddFrontendBlockBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var static $plugin */
    $plugin = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $plugin->entityTypeManager = $container->get('entity_type.manager');
    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'label' => $this->t('Need some help?'),
      'text' => [
        'text' => $this->t('To get listed on Request board click the button below.'),
        'format' => NULL,
      ],
      'button' => $this->t('Request assistance'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getUrl(): Url {
    return Url::fromRoute(
      'node.add',
      ['node_type' => 'request'],
    );
  }

}
