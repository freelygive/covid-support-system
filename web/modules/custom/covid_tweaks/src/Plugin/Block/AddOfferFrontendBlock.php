<?php

namespace Drupal\covid_tweaks\Plugin\Block;

use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an add offer (frontend) block.
 *
 * @Block(
 *   id = "covid_tweaks_add_offer_frontend",
 *   admin_label = @Translation("Add Offer (Frontend)"),
 *   category = @Translation("Covid")
 * )
 */
class AddOfferFrontendBlock extends AddFrontendBlockBase {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var static $plugin */
    $plugin = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $plugin->routeMatch = $container->get('current_route_match');
    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'label' => $this->t('Can you help?'),
      'text' => [
        'text' => $this->t('Let us know how you can help and we can start matching you to people who need support.'),
        'format' => NULL,
      ],
      'button' => $this->t('Offer help'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getUrl(): Url {
    return Url::fromRoute('covid_tweaks.volunteer_profile');
  }

}
