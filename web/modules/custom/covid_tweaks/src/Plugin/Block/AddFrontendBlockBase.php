<?php

namespace Drupal\covid_tweaks\Plugin\Block;

use Drupal\Core\Access\AccessManagerInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\decoupled_auth\Entity\DecoupledAuthUser;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for add frontend cards which handles configuration and redirects.
 */
abstract class AddFrontendBlockBase extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The access manager.
   *
   * @var \Drupal\Core\Access\AccessManagerInterface
   */
  protected $accessManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * AddFrontendBlockBase constructor.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Access\AccessManagerInterface $access_manager
   *   The access manager service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccessManagerInterface $access_manager, AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->accessManager = $access_manager;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('access_manager'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'label' => '',
      'text' => [
        'text' => '',
        'format' => NULL,
      ],
      'button' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['text'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Text'),
      '#format' => $this->configuration['text']['format'],
      '#default_value' => $this->configuration['text']['text'],
    ];
    $form['button'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button text'),
      '#default_value' => $this->configuration['button'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['text'] = $form_state->getValue('text');
    $this->configuration['button'] = $form_state->getValue('button');
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    // If the check and the current user is anonymous, check as an authenticated
    // person.
    if ($this->currentUser->isAnonymous() && $account->isAnonymous()) {
      $values = [
        'roles' => [
          AccountInterface::AUTHENTICATED_ROLE,
          'crm_indiv',
        ],
      ];

      $account = new class($values, 'user') extends DecoupledAuthUser {

        /**
         * This user is not anonymous
         */
        public function isAnonymous() {
          return FALSE;
        }

        /**
         * This user is authenticated.
         */
        public function isAuthenticated() {
          return TRUE;
        }

      };
    }

    $url = $this->getUrl();
    return $this->accessManager
      ->checkNamedRoute(
        $url->getRouteName(),
        $url->getRouteParameters(),
        $account,
        TRUE
      );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $url = $this->getUrl();

    // If the current user is anonymous, we want to direct to the registration
    // page with a redirect.
    if ($this->currentUser->isAnonymous()) {
      $url = Url::fromRoute('user.register', [], [
        'query' => [
          'destination' => $url->toString(),
        ],
      ]);
    }

    $attributes = $url->getOption('attributes');
    $attributes['class'][] = 'btn';
    $attributes['class'][] = 'btn-primary';
    $url->setOption('attributes', $attributes);

    $link = Link::fromTextAndUrl($this->configuration['button'], $url);

    return [
      'content' => [
        '#type' => 'processed_text',
        '#text' => $this->configuration['text']['text'],
        '#format' => $this->configuration['text']['format'],
      ],
      'button' => $link->toRenderable(),
    ];
  }

  /**
   * Get the primary URL for the block.
   *
   * @return \Drupal\Core\Url
   *   The Url.
   */
  abstract protected function getUrl(): Url;

}
