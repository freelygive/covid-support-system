<?php

namespace Drupal\covid_tweaks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an add dbs status block admin.
 *
 * @Block(
 *   id = "covid_add_request_block",
 *   admin_label = @Translation("Add New Request"),
 *   category = @Translation("Covid"),
 *   context = {
 *     "user" = @ContextDefinition("entity:user", required = TRUE, label =
 *   @Translation("User"))
 *   }
 * )
 */
class AddRequestBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Node storage.
   *
   * @var \Drupal\Core\Entity\ContentEntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * Constructs a new StatusArchiveWorker object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->nodeStorage = $entity_type_manager->getStorage('node');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $block = [];

    /* @var \Drupal\user\Entity\User $user */
    $user = $this->getContextValue('user');
    $querystring = \Drupal::destination()->getAsArray();

    $block['add'] = [
      '#type' => 'link',
      '#title' => $this->t('+ Add New Request'),
      '#url' => Url::fromRoute('covid_tweaks.request.public'),
      '#attributes' => [
        'class' => ['btn', 'btn-primary', 'clearfix'],
      ],
    ];

    return $block;
  }

}
