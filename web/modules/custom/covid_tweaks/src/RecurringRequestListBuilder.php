<?php

namespace Drupal\covid_tweaks;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Recurring request entities.
 *
 * @ingroup covid_tweaks
 */
class RecurringRequestListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Recurring request ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\covid_tweaks\Entity\RecurringRequest $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.recurring_request.edit_form',
      ['recurring_request' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
