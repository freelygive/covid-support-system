<?php

namespace Drupal\covid_tweaks\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Offer to help form specifically for adding dispatchers.
 */
class DispatcherOfferForm extends PublicOfferForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'covid_tweaks_dispatcher_offer_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $group_name = $this->currentUserEntity()
      ->get('organisations')
      ->first()->entity->getGroup()->label();
    $form['assign_to_group_top'] = $form['assign_to_group_bottom'] = [
      '#type' => 'item',
      '#wrapper_attributes' => [
        'class' => ['alert', 'alert-info'],
      ],
      '#markup' => $this->t('This offer will be assigned to %label.', [
        '%label' => $group_name,
      ]),
      '#default_value' => TRUE,
      '#weight' => 99,
    ];
    $form['assign_to_group_top']['#weight'] = -99;

    $form['assign_as_dispatcher'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Make this contact a dispatcher for %label', [
        '%label' => $group_name,
      ]),
      '#description' => $this->t('As a dispatcher, this contact will be able to manage other dispatchers and offers for the organisation.'),
      '#default_value' => TRUE,
      '#weight' => 99,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Show an error if the user already has a group.
    if (!$this->user->isNew() && $this->user->organisations->count()) {
      $form_state->setErrorByName('', $this->t('A contact with this email already exists in the system and is already part of an organisation. Being part of multiple organisations is not currently supported.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $current_user = $this->currentUserEntity();

    // If the current user is a dispatcher, assign the volunteer to the group
    // unless they are adding an offer for themselves. In this case, they must
    // already be a member to be a dispatcher so no action is needed.
    if ($current_user->id() != $this->user->id()) {
      /** @var \Drupal\group\Entity\GroupContentInterface $group_content */
      $group_content = $this->entityTypeManager
        ->getStorage('group_content')
        ->create([
          'type' => 'contacts_org-group_membership',
          'gid' => $current_user->get('organisations')->first()->entity->gid,
          'entity_id' => $this->user->id(),
        ]);

      if ($form_state->getValue('assign_as_dispatcher')) {
        $group_content->set('group_roles', ['contacts_org-dispatcher']);
      }

      $group_content->save();

      // Redirect to the offers dashboard.
      $form_state->setRedirect('view.organisation_offers.offers', [
        'user' => $current_user->id(),
      ]);
    }
  }

}
