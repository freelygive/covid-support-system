<?php

namespace Drupal\covid_tweaks\Form;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use Drupal\search_api\IndexInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for auto assigning requests to users.
 */
class AutoAssignForm extends FormBase {

  /**
   * Search API Index storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $searchApiIndexStorage;

  /**
   * User storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $userStorage;

  /**
   * Node storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * The name of the combined location field.
   *
   * @var string
   */
  protected $fieldName = 'location_switch';

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $static = parent::create($container);

    $static->searchApiIndexStorage = $container->get('entity_type.manager')->getStorage('search_api_index');
    $static->userStorage = $container->get('entity_type.manager')->getStorage('user');
    $static->nodeStorage = $container->get('entity_type.manager')->getStorage('node');

    return $static;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'node.request.auto-assign';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->buildFilters($form, $form_state);

    $form['table'] = [
      '#type' => 'table',
      '#header' => [
        'label' => $this->t('Request'),
        'contact' => $this->t('Person'),
        'address' => $this->t('Address'),
        'assign_to' => $this->t('Assign to'),
      ],
      '#tree' => TRUE,
    ];

    // Remove any user input for assignees so that we recalculate every time
    // in case the filter values have changed.
    $form_state->setUserInput(['table' => []]);

    $filter_distance = $form_state->getValue('distance') ?? 5;
    $filter_assigned = $form_state->getValue('assigned_count') ?? 5;
    $nearest_assignees = $form_state->getValue('nearest_assignees') ?? 5;
    $assign_counts = [];

    $request_query = $this->getAssignableRequestsQuery($form_state);
    $requests = $this->nodeStorage->loadMultiple($request_query->execute());

    /** @var \Drupal\node\NodeInterface $request */
    foreach ($requests as $request) {
      $profile = $request->getOwner()->get('profile_crm_indiv')->entity;
      if ($profile) {
        $options = $this->findAutomaticAssignees($request, $filter_distance, $filter_assigned, $nearest_assignees, $assign_counts);
        $default = array_keys($options)[0];

        $form['table'][$request->id()] = [
          '#type' => 'select',
          '#id' => 'assign_' . $request->id(),
          '#title' => $this->t('Assign to'),
          '#options' => $options,
          // Do not check form state as this prevents recalculation after
          // changing the filter settings.
          '#default_value' => $default,
        ];

        $form['table']['#rows'][$request->id()] = [
          'label' => $request->label(),
          'contact' => $request->getOwner()->label(),
          'address' => '',
          'assign_to' => [
            'data' => &$form['table'][$request->id()],
          ],
        ];
        if ($profile && !$profile->get('crm_address')->isEmpty()) {
          $form['table']['#rows'][$request->id()]['address'] = [
            'data' => $profile->get('crm_address')->first()->view(),
          ];
        }
      }
    }

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Auto assign'),
    ];
    return $form;
  }

  /**
   * Helper function to build filter options.
   *
   * @param array $form
   *   The form being built.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  protected function buildFilters(array &$form, FormStateInterface $form_state) {
    $form['filters'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Filters'),
    ];

    $form['filters']['distance'] = [
      '#type' => 'number',
      '#step' => 0.1,
      '#title' => $this->t('Distance limit'),
      '#description' => $this->t('Do not automatically assign to anyone further than this distance from the request.'),
      '#field_suffix' => new FormattableMarkup(' @text', [
        '@text' => $this->t('miles'),
      ]),
      '#default_value' => $form_state->getValue('distance') ?? 5,
      '#weight' => 5,
    ];

    $form['filters']['assigned_count'] = [
      '#type' => 'number',
      '#title' => $this->t('Already assigned'),
      '#description' => $this->t('Do not automatically assign to anyone who already has this many open requests (or more).'),
      '#default_value' => $form_state->getValue('assigned_count') ?? 5,
      '#weight' => 10,
    ];

    $form['filters']['nearest_assignees'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of options'),
      '#description' => $this->t('The number of assignee options to provide for each request. The default will be the nearest that meets the other criteria.'),
      '#default_value' => $form_state->getValue('nearest_assignees') ?? 5,
      '#weight' => 15,
    ];

    $form['filters']['actions'] = [
      'filter' => [
        '#type' => 'submit',
        '#value' => $this->t('Filter'),
        '#submit' => [
          [self::class, 'filterSubmit'],
        ],
      ],
      '#weight' => 30,
    ];
  }

  /**
   * Submit handler for filter button. Rebuilds the form.
   *
   * @param array $form
   *   The form being submitted.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state being submitted.
   */
  public static function filterSubmit(array $form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue('table');

    // Load all Requests and Users together for performance.
    $requests = $this->nodeStorage->loadMultiple(array_keys($values));
    $user_ids = array_filter($values, function ($user_id) {
      return $user_id != '_do_not_assign';
    });
    $users = $this->userStorage->loadMultiple($user_ids);

    foreach ($requests as $request_id => $request) {
      if (empty($users[$values[$request_id]])) {
        $this->messenger()->addStatus($this->t('Request %request was not assigned as specified.', [
          '%request' => $request->label() . ' [' . $request->id() . ']',
        ]));
        continue;
      }
      $user = $users[$values[$request_id]];

      $assignees = [
        ['target_id' => $user->id()],
      ];
      $request->set('field_assignees', $assignees);
      $request->set('field_status', 'assigned');
      $request->save();

      $this->messenger()->addStatus($this->t('Request %request has been assigned to %user.', [
        '%request' => $request->label() . ' [' . $request->id() . ']',
        '%user' => $user->label() . ' [' . $user->id() . ']',
      ]));

      $this->getLogger('covid_tweaks')->notice('Request %request has been assigned to %user.', [
        '%request' => $request->label() . ' [' . $request->id() . ']',
        '%user' => $user->label() . ' [' . $user->id() . ']',
      ]);
    }
  }

  /**
   * Get a query for assignable requests to be auto-assigned.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state object.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   A query to get assignable requests.
   */
  protected function getAssignableRequestsQuery(FormStateInterface $form_state) {
    return \Drupal::entityQuery('node')
      ->condition('type', 'request')
      ->notExists('field_assignees')
      ->condition('status', TRUE)
      ->condition('field_status', 'requested');
  }

  /**
   * Calculate the automatic assignee options for a request.
   *
   * @param \Drupal\node\NodeInterface $request
   *   The request to find assignees for.
   * @param int $filter_distance
   *   The radius of the search in miles.
   * @param int $assigned_max
   *   The maximum number of already assigned requests a user can have.
   * @param int $nearest_assignees
   *   The number of options to show for each Request.
   * @param int[] $assign_counts
   *   Pending assignments, keyed by user ID.
   *
   * @return array
   *   An array of Users who could be assigned to the request.
   */
  protected function findAutomaticAssignees(NodeInterface $request, int $filter_distance, int $assigned_max, int $nearest_assignees, array &$assign_counts) {
    $options = [];

    $index = $this->searchApiIndexStorage->load('contacts_index');
    $query = $this->getAssigneeOptionsQuery($request, $index, $filter_distance, $assigned_max, $nearest_assignees, $assign_counts);
    $results = $query->execute();

    $user_ids = [];
    foreach ($results->getResultItems() as $index => $result_item) {
      $user_id = substr($index, strlen('entity:user/'), 0 - strlen(':en'));
      $distance_field = $result_item->getField($this->fieldName . '__distance');
      $user_ids[$user_id] = [
        'distance' => $distance_field ? $distance_field->getValues()[0] : NULL,
      ];
    }
    $users = $this->userStorage->loadMultiple(array_keys($user_ids));
    $matches = 0;
    $deferred_options = [];
    foreach ($users as $user) {
      if (isset($user_ids[$user->id()]['distance'])) {
        $distance_miles = $user_ids[$user->id()]['distance'] * 0.621371;
        $distance_text = $this->t('@number miles', [
          '@number' => number_format($distance_miles, 2),
        ]);
      }
      else {
        $distance_text = $this->t('distance unknown');
      }
      $option_label = new FormattableMarkup('@label (@distance)', [
        '@label' => $user->label(),
        '@distance' => $distance_text,
      ]);

      // If we are over our assign limit for this user, put them at the bottom
      // of the list (after none) so they aren't automatically over allocated.
      $assigns_including_pending = $user->get('assigned_requests')->count() + ($assign_counts[$user->id()] ?? 0);
      if ($assigns_including_pending >= $assigned_max) {
        $deferred_options[$user->id()] = $option_label;
      }
      else {
        // Only track the assignment for the first option.
        if (empty($options)) {
          $assign_counts[$user->id()] = ($assign_counts[$user->id()] ?? 0) + 1;
        }
        $options[$user->id()] = $option_label;
      }

      $matches++;
      if ($matches >= $nearest_assignees) {
        break;
      }
    }

    // Always include Do not assign option.
    $options['_do_not_assign'] = $this->t('Do not assign');

    return $options + $deferred_options;
  }

  /**
   * Get a query to run for the assignee options for a specific request.
   *
   * @param \Drupal\node\NodeInterface $request
   *   The request to get assignee options for.
   * @param \Drupal\search_api\IndexInterface $index
   *   The search index entity to search.
   * @param int $filter_distance
   *   The radius of the search in miles.
   * @param int $assigned_max
   *   The maximum number of already assigned requests a user can have.
   * @param int $nearest_assignees
   *   The number of options to show for each Request.
   * @param int[] $assign_counts
   *   Pending assignments, keyed by user ID.
   *
   * @return \Drupal\search_api\Query\QueryInterface
   *   The query to run to get assignee options.
   */
  protected function getAssigneeOptionsQuery(NodeInterface $request, IndexInterface $index, int $filter_distance, int $assigned_max, int $nearest_assignees, array &$assign_counts) {
    /** @var \Drupal\search_api\Query\QueryInterface $query */
    $query = $index->query();
    $query->addCondition('assigned_request_count', $assigned_max, '<');
    $query->addCondition('field_are_you_looking_to_offer_o', 'volunteer');
    // Limit the assignees, but with a buffer as we'll exclude pending
    // assignees.
    $query->range(0, $nearest_assignees + count($assign_counts));

    if ($geolocation = $this->getGeolocation($request)) {
      $location_options = [
        'field' => $this->fieldName,
        'lat' => $geolocation['lat'],
        'lon' => $geolocation['lon'],
        'radius' => $filter_distance * 1.60934,
      ];

      $query->setOption('search_api_location', [$location_options]);
      $query->sort($this->fieldName . '__distance', 'ASC');
    }

    return $query;
  }

  /**
   * Get the geolocation field value for a request.
   *
   * @param \Drupal\node\NodeInterface $request
   *   The request.
   *
   * @return array|null
   *   The geolocation array, or NULL if none is available.
   */
  protected function getGeolocation(NodeInterface $request): ?array {
    $owner = $request->getOwner();
    if (!$owner) {
      return NULL;
    }

    $indiv_profile = $owner->get('profile_crm_indiv')->entity;
    if (!$indiv_profile) {
      return NULL;
    }

    $geolocation = $indiv_profile->get('geolocation_geocoded');
    if ($geolocation->isEmpty()) {
      return NULL;
    }

    return $geolocation->first()->getValue();
  }

}
