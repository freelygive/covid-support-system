<?php

namespace Drupal\covid_tweaks\Form;

use Drupal\contacts\Form\AddContactBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Create a request without logging in form.
 */
class PublicRequestForm extends AddContactBase {

  use ContactFormTrait;

  /**
   * The request entity.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $request;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'covid_tweaks_public_request_form';
  }

  /**
   * Check the default access level for requests.
   *
   * @return bool
   *   Whether the current user has access.
   */
  protected function hasGroupAccess(): bool {
    return $this->hasGroupPermissions(['view group_node:request entity']);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    // Get the profile and request entities.
    /** @var \Drupal\profile\Entity\ProfileInterface $profile */
    $profile = $this->entityTypeManager->getStorage('profile')->create(['type' => 'crm_indiv']);
    $profile->setOwnerId($this->currentUser()->id());
    $profile_fields = $this->entityFieldManager->getFieldDefinitions('profile', 'crm_indiv');
    $profile_fields['crm_name']->setRequired(TRUE);
    // If the user doesn't have administer users permission or view group
    // membership content group permission then address is required.
    if (!$this->hasGroupPermissions(['view group_membership content']) && !$this->currentUser()->hasPermission('administer users')) {
      $profile_fields['crm_address']->setRequired(TRUE);
    }

    /** @var \Drupal\node\NodeInterface $request */
    $request = $this->entityTypeManager->getStorage('node')->create(['type' => 'request']);
    $request->setOwnerId($this->currentUser()->id());
    $request_fields = $this->entityFieldManager->getFieldDefinitions('node', 'request');
    $request_fields['field_tags']->setRequired(TRUE);

    $fields = [
      // Profile: Name.
      'crm_name' => [
        'entity' => 'profile',
        'weight' => 0,
      ],
      // User: Email.
      // @see: \Drupal\contacts\Form\AddContactBase::buildForm
      // Profile: Phone.
      'crm_phone' => [
        'entity' => 'profile',
        'weight' => $weight = 2,
      ],
      // Profile: Address.
      'crm_address' => [
        'entity' => 'profile',
        'weight' => ++$weight,
      ],
      // Request: Tags.
      'field_tags' => [
        'entity' => 'request',
        'weight' => ++$weight,
        'config' => [
          'type' => 'select_or_other_reference',
          'settings' => [
            'select_element_type' => 'select_or_other_buttons',
          ],
        ],
      ],
      // Request: Title.
      'title' => [
        'entity' => 'request',
        'weight' => ++$weight,
      ],
      // Request: Due date.
      'field_date' => [
        'entity' => 'request',
        'weight' => ++$weight,
      ],
      // Request: Body.
      'body' => [
        'entity' => 'request',
        'weight' => ++$weight,
      ],
      // Profile: Employment status.
      'field_employment_status' => [
        'entity' => 'profile',
        'weight' => ++$weight,
        'config' => [
          'type' => 'options_buttons',
        ],
      ],
      // Profile: Languages.
      'field_languages' => [
        'entity' => 'profile',
        'weight' => ++$weight,
        'config' => [
          'type' => 'select_or_other_list',
          'settings' => [
            'select_element_type' => 'select_or_other_buttons',
          ],
        ],
      ],
      // Profile: Limitations.
      'field_limitations' => [
        'entity' => 'profile',
        'weight' => ++$weight,
        'config' => [
          'type' => 'select_or_other_list',
          'settings' => [
            'select_element_type' => 'select_or_other_buttons',
          ],
        ],
      ],
    ];

    // Load the entity display and check for fields in the tags field groups.
    /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface $display */
    $display = $this->entityTypeManager->getStorage('entity_form_display')->load('node.request.default');
    $data = $display->getThirdPartySettings('field_group');
    $tag_fields = [];
    foreach ($data as $group_name => $group) {
      if (!empty($group['children'])) {
        foreach ($group['children'] as $field_name) {
          $tag_fields[$field_name] = $group_name;
        }
      }
    }

    // Add tag sub question fields.
    if (!empty($tag_fields)) {
      $tags_weight = $fields['field_tags']['weight'] + 0.1;
      foreach ($tag_fields as $field_name => $group_name) {
        if ($component = $display->getComponent($field_name)) {
          $fields[$field_name] = [
            'entity' => 'request',
            'weight' => $tags_weight,
          ];
        }
      }
    }

    foreach ($fields as $field_name => $field_info) {
      $entity = ${$field_info['entity']};
      $field_definitions = ${$field_info['entity'] . '_fields'};
      $form[$field_name] = $this->getWidgetForm($entity, $field_definitions, $field_name, $form, $form_state, $field_info['config'] ?? []);
      $form[$field_name]['#weight'] = $field_info['weight'];
      $form[$field_name]['#entity_namespace'] = $field_info['entity'];
    }

    // Make adjustments beyond widget config.
    $form['crm_name']['widget'][0]['#theme_wrappers'] = [];
    unset($form['crm_address']['widget'][0]['#type']);
    unset($form['mail']['widget'][0]['value']['#description']);
    $form['field_tags']['widget']['#title'] = $this->t('Type of request');
    $form['title']['widget'][0]['value']['#title'] = $this->t('Brief description of your request');
    $form['body']['widget'][0]['#title'] = $this->t('Additional information');
    unset($form['field_tags']['widget']['#description']);

    // Make the tag fields conditional on term selection.
    if (!empty($tag_fields)) {
      foreach ($tag_fields as $field_name => $group_name) {
        if (!empty($form[$field_name])) {
          $term_id = trim($group_name, 'group_tags_');
          $term = $this->entityTypeManager->getStorage('taxonomy_term')->load($term_id);
          $form[$field_name]['#states'] = [
            'visible' => [
              ':input[name="field_tags[select][' . $term->label() . ' (' . $term_id . ')]"]' => ['checked' => TRUE],
            ],
          ];
        }
      }
    }

    // We only want to show the checkbox if we have submitted the form and the
    // email acquires an existing user.
    $input = $form_state->getUserInput();
    if (!empty($input['mail'][0]['value'])) {
      $email = $input['mail'][0]['value'];
      $contact = $this->acquire($email);
      if (!empty($contact)) {
        $form['confirm_acquisition'] = [
          '#type' => 'checkbox',
          '#title' => new TranslatableMarkup('Please use the contact details on file for the above entered email address.'),
          '#weight' => 99,
        ];
      }
    }

    if ($this->hasGroupAccess()) {
      $form['assign_to_group'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('This request is part of %label', [
          '%label' => $this->currentUserEntity()->get('organisations')->first()->entity->getGroup()->label(),
        ]),
        '#description' => $this->t('If not checked, this request will be made public and other organisations and individuals will be able to offer to meet the request.'),
        '#default_value' => TRUE,
        '#weight' => 99,
      ];
    }
    else {
      $form['assign_to_group'] = [
        '#type' => 'value',
        '#value' => FALSE,
      ];
    }

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Show a validation error if the contact already exists and the user has
    // not checked the confirmation checkbox.
    if (!$this->user->isNew() && !$form_state->getValue('confirm_acquisition')) {
      // Check fields.
      $message = new TranslatableMarkup("Our records indicate that this is not your first request.<br>
      For data protection reasons, we will only use the contact details that we already have on file for you.<br>
      To change your details, simply log in or register an account with the same email and update your information.<br>
      <strong>To continue please select the checkbox to confirm you have read this and submit the form.</strong>");
      $form_state->setErrorByName('', $message);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function buildEntities(array $form, FormStateInterface $form_state) {
    // Let the trait build the user and profile.
    $this->buildUserAndProfile($form, $form_state);

    // Create the request node.
    $this->request = $this->entityTypeManager
      ->getStorage('node')
      ->create(['type' => 'request']);

    $should_validate = !$form_state->getTemporaryValue('entity_validated');
    $this->request->setValidationRequired($should_validate);
    $fields = $this->entityFieldManager->getFieldDefinitions('node', 'request');

    // Populate values on the request.
    foreach (Element::children($form) as $field_name) {
      // We're only interested in the request namespace.
      if ('request' !== ($form[$field_name]['#entity_namespace'] ?? NULL)) {
        continue;
      }

      $widget = $this->getWidget($fields, $field_name);
      $widget->extractFormValues($this->request->get($field_name), $form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->request->setOwner($this->user);
    $this->request->save();

    $current_user = $this->currentUserEntity();

    // By default redirect to the user front page/user dashboard.
    if ($current_user->isAnonymous()) {
      $form_state->setRedirect('<front>');
    }
    else {
      $form_state->setRedirect('contacts_user_dashboard.summary', [
        'user' => $this->user->id(),
      ]);
    }

    // If selected, assign the request to the group.
    if ($form_state->getValue('assign_to_group')) {
      $group_content = $this->entityTypeManager
        ->getStorage('group_content')
        ->create([
          'type' => 'contacts_org-group_node-request',
          'gid' => $current_user->get('organisations')->first()->entity->gid,
          'entity_id' => $this->request->id(),
        ]);
      $group_content->save();
    }

    // Redirect to the requests dashboard if the offer isn't for the current
    // user, but the current user is a dispatcher.
    if ($current_user->id() != $this->user->id() && $this->hasGroupAccess()) {
      $form_state->setRedirect('view.profile_requests.page_1', [
        'user' => $current_user->id(),
      ]);
    }

    $this->messenger()->addMessage('Thank you for your request. It has been received and someone will be in touch shortly.');
  }

}
