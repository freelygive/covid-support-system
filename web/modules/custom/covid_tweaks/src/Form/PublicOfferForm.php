<?php

namespace Drupal\covid_tweaks\Form;

use Drupal\contacts\Form\AddContactBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Public offer to help form.
 */
class PublicOfferForm extends AddContactBase {

  use ContactFormTrait;

  /**
   * The volunteer profile.
   *
   * @var \Drupal\profile\Entity\ProfileInterface
   */
  protected $volunteerProfile;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'covid_tweaks_public_offer_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    // Get the profile entity.
    /** @var \Drupal\profile\Entity\ProfileInterface $profile */
    $profile = $this->entityTypeManager->getStorage('profile')->create(['type' => 'crm_indiv']);
    $profile->setOwnerId($this->currentUser()->id());
    $profile_fields = $this->entityFieldManager->getFieldDefinitions('profile', 'crm_indiv');
    $profile_fields['crm_name']->setRequired(TRUE);
    // If the user doesn't have administer users permission or view group
    // membership content group permission then address is required.
    if (!$this->hasGroupPermissions(['view group_membership content']) && !$this->currentUser()->hasPermission('administer users')) {
      $profile_fields['crm_address']->setRequired(TRUE);
    }

    /** @var \Drupal\profile\Entity\ProfileInterface $profile */
    $volunteer_profile = $this->entityTypeManager->getStorage('profile')->create(['type' => 'volunteer']);
    $volunteer_profile->setOwnerId($this->currentUser()->id());
    $volunteer_profile_fields = $this->entityFieldManager->getFieldDefinitions('profile', 'volunteer');

    $fields = [
      // Profile: Name.
      'crm_name' => [
        'entity' => 'profile',
        'weight' => 0,
      ],
      // User: Email.
      // @see: \Drupal\contacts\Form\AddContactBase::buildForm
      // Profile: Phone.
      'crm_phone' => [
        'entity' => 'profile',
        'weight' => $weight = 2,
      ],
      // Profile: Address.
      'crm_address' => [
        'entity' => 'profile',
        'weight' => ++$weight,
      ],
      'field_ways_that_i_can_help' => [
        'entity' => 'volunteer_profile',
        'weight' => ++$weight,
        'config' => [
          'type' => 'options_buttons',
        ],
      ],
      // Profile: Essential worker.
      'field_essential_worker' => [
        'entity' => 'volunteer_profile',
        'weight' => ++$weight,
        'config' => [
          'type' => 'options_buttons',
        ],
      ],
      // Profile: Languages.
      'field_languages' => [
        'entity' => 'profile',
        'weight' => ++$weight,
        'config' => [
          'type' => 'select_or_other_list',
          'settings' => [
            'select_element_type' => 'select_or_other_buttons',
          ],
        ],
      ],
    ];

    // Load the entity display and check for fields in the tags field groups.
    /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface $display */
    $display = $this->entityTypeManager->getStorage('entity_form_display')->load('profile.volunteer.default');
    $data = $display->getThirdPartySettings('field_group');
    $tag_fields = [];
    foreach ($data as $group_name => $group) {
      if (!empty($group['children'])) {
        foreach ($group['children'] as $field_name) {
          $tag_fields[$field_name] = $group_name;
        }
      }
    }

    // Add tag sub question fields.
    if (!empty($tag_fields)) {
      $tags_weight = $fields['field_ways_that_i_can_help']['weight'] + 0.1;
      foreach ($tag_fields as $field_name => $group_name) {
        if ($component = $display->getComponent($field_name)) {
          $fields[$field_name] = [
            'entity' => 'volunteer_profile',
            'weight' => $tags_weight,
          ];
        }
      }
    }

    foreach ($fields as $field_name => $field_info) {
      $entity = ${$field_info['entity']};
      $field_definitions = ${$field_info['entity'] . '_fields'};
      $form[$field_name] = $this->getWidgetForm($entity, $field_definitions, $field_name, $form, $form_state, $field_info['config'] ?? []);
      $form[$field_name]['#weight'] = $field_info['weight'];
      $form[$field_name]['#entity_namespace'] = $field_info['entity'];
    }

    // Make adjustments beyond widget config.
    $form['crm_name']['widget'][0]['#theme_wrappers'] = [];
    unset($form['crm_address']['widget'][0]['#type']);
    unset($form['mail']['widget'][0]['value']['#description']);

    // Make the tag fields conditional on term selection.
    if (!empty($tag_fields)) {
      foreach ($tag_fields as $field_name => $group_name) {
        if (!empty($form[$field_name])) {
          $term_id = trim($group_name, 'group_tags_');
          $form[$field_name]['#states'] = [
            'visible' => [
              ':input[name="field_ways_that_i_can_help[' . $term_id . ']"]' => ['checked' => TRUE],
            ],
          ];
        }
      }
    }

    // We only want to show the checkbox if we have submitted the form and the
    // email acquires an existing user.
    $input = $form_state->getUserInput();
    if (!empty($input['mail'][0]['value'])) {
      $email = $input['mail'][0]['value'];
      $contact = $this->acquire($email);
      if (!empty($contact)) {
        $form['confirm_acquisition'] = [
          '#type' => 'checkbox',
          '#title' => new TranslatableMarkup('Please use the contact details on file for the above entered email address.'),
          '#weight' => 99,
        ];
      }
    }

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    if (!$this->volunteerProfile->isNew() && !in_array($this->volunteerProfile->volunteer_status->value, ['withdrawn', 'no_contact'])) {
      $message = new TranslatableMarkup("Our records indicate that you have already made an offer to help.<br>
        To change your details, simply log in or register an account with the same email address and update your information.");
      $form_state->setErrorByName('', $message);
    }
    // Show a validation error if the contact already exists and the user has
    // not checked the confirmation checkbox.
    elseif (!$this->user->isNew() && !$form_state->getValue('confirm_acquisition')) {
      // Check fields.
      $message = new TranslatableMarkup("Our records indicate that you have already made an offer to help.<br>
      For data protection reasons, we will only use the contact details that we already have on file for you.<br>
      To change your details, simply log in or register an account with the same email and update your information.<br>
      <strong>To continue please select the checkbox to confirm you have read this and submit the form.</strong>");
      $form_state->setErrorByName('', $message);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function buildEntities(array $form, FormStateInterface $form_state) {
    // Let the trait build the user and profile.
    $this->buildUserAndProfile($form, $form_state);

    // Ensure that this user is set to be a volunteer.
    $this->profile->set('field_are_you_looking_to_offer_o', ['volunteer']);

    // If the user doesnt have a volunteer profile, create one.
    if ($this->user->get('profile_volunteer')->isEmpty()) {
      /* @var \Drupal\profile\Entity\ProfileInterface $profile */
      $this->volunteerProfile = $this->entityTypeManager->getStorage('profile')->create([
        'type' => 'volunteer',
        'status' => TRUE,
        'is_default' => TRUE,
      ]);
    }
    else {
      $this->volunteerProfile = $this->user->get('profile_volunteer')->entity;
    }

    $should_validate = !$form_state->getTemporaryValue('entity_validated');
    $this->volunteerProfile->setValidationRequired($should_validate);
    $fields = $this->entityFieldManager->getFieldDefinitions('profile', 'volunteer');

    foreach (Element::children($form) as $field_name) {
      // We're only interested in the volunteer namespace.
      if ('volunteer_profile' !== ($form[$field_name]['#entity_namespace'] ?? NULL)) {
        continue;
      }

      $widget = $this->getWidget($fields, $field_name);
      $widget->extractFormValues($this->volunteerProfile->get($field_name), $form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->volunteerProfile->setOwner($this->user);
    $this->volunteerProfile->set('volunteer_status', 'submitted');
    $this->volunteerProfile->save();

    $current_user = $this->currentUserEntity();

    // By default redirect to the user front page/user dashboard.
    if ($current_user->isAnonymous()) {
      $form_state->setRedirect('<front>');
    }
    else {
      $form_state->setRedirect('contacts_user_dashboard.summary', [
        'user' => $this->user->id(),
      ]);
    }

    $this->messenger()->addMessage('Thank you for your offer. It has been received and someone will be in touch shortly.');
  }

}
