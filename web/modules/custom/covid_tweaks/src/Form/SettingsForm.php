<?php

namespace Drupal\covid_tweaks\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Covid Tweaks settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Privacy - Names: Shown publicly.
   */
  const PRIVACY_NAMES_PUBLIC = 'public';

  /**
   * Privacy - Names: Only shown when assigned.
   */
  const PRIVACY_NAMES_ASSIGNED = 'assigned';

  /**
   * The Search API Index entity storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $indexStorage;

  /**
   * Constructs a the form object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\Entity\ConfigEntityStorageInterface $index_storage
   *   The Search API Index entity storage.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ConfigEntityStorageInterface $index_storage) {
    parent::__construct($config_factory);
    $this->indexStorage = $index_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')->getStorage('search_api_index')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'covid_tweaks_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['covid_tweaks.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('covid_tweaks.settings');
    $form['#tree'] = TRUE;

    $form['privacy'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Privacy settings'),
    ];

    $form['privacy']['names'] = [
      '#type' => 'radios',
      '#title' => $this->t('Show names'),
      '#default_value' => $config->get('privacy.names'),
      '#description' => $this->t('Staff will always be able to see full details of all contacts.'),
      '#required' => TRUE,
      '#options' => [
        self::PRIVACY_NAMES_PUBLIC => $this->t('Publicly show the requester and volunteer names'),
        self::PRIVACY_NAMES_ASSIGNED => $this->t('Only show names for assigned requests'),
      ],
      self::PRIVACY_NAMES_ASSIGNED => [
        '#description' => $this->t("The assignee will be able to see the requester's name and vice versa."),
      ],
    ];

    $form['privacy']['location']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Obfuscate locations'),
      '#description' => $this->t('If enabled, locations will be rounded to reduce the resolution of the locations on a may and in distance based queries.'),
      '#default_value' => $config->get('privacy.location.enabled'),
    ];
    $form['privacy']['location']['precision'] = [
      '#type' => 'number',
      '#title' => $this->t('Decimal places'),
      '#description' => $this->t('See @link for the resolution of various decimal places.', [
        '@link' => Markup::create('<a href="https://en.wikipedia.org/wiki/Decimal_degrees#Precision" target="_blank">https://en.wikipedia.org/wiki/Decimal_degrees#Precision</a>'),
      ]),
      '#default_value' => $config->get('privacy.location.precision'),
      '#min' => 0,
      '#max' => 8,
      '#states' => [
        'visible' => [':input[name="privacy[location][enabled]"]' => ['checked' => TRUE]],
        'required' => [':input[name="privacy[location][enabled]"]' => ['checked' => TRUE]],
      ],
    ];
    $form['privacy']['location']['divider'] = [
      '#type' => 'number',
      '#title' => $this->t('Divider'),
      '#description' => $this->t('Optionally an additional divider to apply. For example, if set to 2, the last digit will be one of 0, 2, 4, 6 or 8. If 5, the last digit will be one of 0 or 5.'),
      '#default_value' => $config->get('privacy.location.divider'),
      '#min' => 0,
      '#max' => 9,
      '#states' => [
        'visible' => [':input[name="privacy[location][enabled]"]' => ['checked' => TRUE]],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue(['privacy', 'location_enabled'])) {
      $precision = $form_state->getValue(['privacy', 'location', 'precision']);
      if ($precision === NULL || $precision === '') {
        $form_state->setError($form['privacy']['location']['precision'], $this->t('Please provide a precision.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('covid_tweaks.settings');
    $original_location = $config->get('privacy.location') ?? [];

    $config
      ->set('privacy.names', $form_state->getValue(['privacy', 'names']))
      ->set('privacy.location', $form_state->getValue(['privacy', 'location']))
      ->save();
    parent::submitForm($form, $form_state);

    // If location obfuscation has been disabled/enabled or the settings have
    // changed, mark the index for re-indexing.
    $new_location = $config->get('privacy.location');
    if ((!empty($original_location['enabled']) || !empty($new_location['enabled'])) && $original_location != $new_location) {
      /** @var \Drupal\search_api\IndexInterface $index */
      $index = $this->indexStorage->load('contact_mapping');
      $index->reindex();
      $this->messenger()->addStatus($this->t('Marked @label for re-indexing.', [
        '@label' => $index->label(),
      ]));
    }
  }

}
