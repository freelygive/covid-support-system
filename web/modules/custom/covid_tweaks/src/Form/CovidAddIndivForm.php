<?php

namespace Drupal\covid_tweaks\Form;

use Drupal\contacts\Form\AddIndivForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Form for adding a new individual. Extends the base functionality in contacts.
 *
 * @package Drupal\covid_tweaks\Form
 */
class CovidAddIndivForm extends AddIndivForm {

  /**
   * The volunteer profile.
   *
   * @var \Drupal\profile\Entity\Profile
   */
  protected $volunteerProfile;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    // By default the contacts_theme does not show any messages added through
    // Drupal::messenger() or drupal_set_messages. These are only shown on the
    // main dashboard page, so need to explicitly include them.
    $form['messages'] = [
      '#weight' => -1,
      '#type' => 'status_messages',
      '#include_fallback' => TRUE,
    ];

    // Add some additional profile fields.
    $profile_fields = $this->entityFieldManager->getFieldDefinitions('profile', 'crm_indiv');
    $profile = $this->entityTypeManager->getStorage('profile')->create(['type' => 'crm_indiv']);

    $weight = 2;
    $profile_field_names = [
      'crm_dob' => [],
      'crm_phone' => [],
      'crm_address' => [],
      'field_situation' => [],
      'field_are_you_looking_to_offer_o' => ['type' => 'options_buttons'],
    ];
    foreach ($profile_field_names as $field_name => $configuration) {
      $form[$field_name] = $this->getWidgetForm($profile, $profile_fields, $field_name, $form, $form_state, $configuration);
      $form[$field_name]['#weight'] = $weight;
      $form[$field_name]['#entity_namespace'] = 'profile';
      $weight++;
    }
    $form['crm_address']['widget'][0]['#type'] = 'fieldset';

    // Make user email not required.
    $user_fields = $this->entityFieldManager->getFieldDefinitions('user', 'user');
    $user = $this->entityTypeManager->getStorage('user')->create();
    $user_fields['mail']->setRequired(FALSE);
    $form['mail'] = $this->getWidgetForm($user, $user_fields, 'mail', $form, $form_state);
    $form['mail']['#weight'] = 1;
    $form['mail']['#entity_namespace'] = 'user';

    // Add volunteer fields.
    $volunteer_profile = $this->entityTypeManager->getStorage('profile')->create(['type' => 'volunteer']);
    $volunteer_profile_fields = $this->entityFieldManager->getFieldDefinitions('profile', 'volunteer');
    $volunteer_field_names = [
      'field_useful_skills_that_i_can_o' => ['type' => 'options_buttons'],
      'field_ways_that_i_can_help' => ['type' => 'options_buttons'],
    ];
    foreach ($volunteer_field_names as $field_name => $configuration) {
      $form[$field_name] = $this->getWidgetForm($volunteer_profile, $volunteer_profile_fields, $field_name, $form, $form_state, $configuration);
      $form[$field_name]['#weight'] = $weight;
      $form[$field_name]['#entity_namespace'] = 'profile_volunteer';
      $form[$field_name]['#states']['visible'] = [
        ':input[name="field_are_you_looking_to_offer_o[volunteer]"]' => ['checked' => TRUE],
      ];
      $weight++;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildEntities(array $form, FormStateInterface $form_state) {
    parent::buildEntities($form, $form_state);

    /* @var \Drupal\profile\Entity\ProfileInterface $profile */
    $profile = $this->entityTypeManager->getStorage('profile')->create([
      'type' => 'volunteer',
    ]);
    $profile->setValidationRequired(!$form_state->getTemporaryValue('entity_validated'));
    $profile_fields = $this->entityFieldManager->getFieldDefinitions('profile', 'volunteer');

    foreach (Element::children($form) as $field_name) {
      if ('profile_volunteer' !== ($form[$field_name]['#entity_namespace'] ?? NULL)) {
        continue;
      }
      $widget = $this->getWidget($profile_fields, $field_name);
      $widget->extractFormValues($profile->get($field_name), $form, $form_state);
    }

    $this->volunteerProfile = $profile;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->volunteerProfile->setOwner($this->user);
    $this->volunteerProfile->save();
  }

}
