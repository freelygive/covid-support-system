<?php

namespace Drupal\covid_tweaks\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\decoupled_auth\AcquisitionService;
use Drupal\user\UserInterface;

/**
 * A trait to simplify COVID contact forms.
 *
 * @todo: Put some of this back into base.
 */
trait ContactFormTrait {

  /**
   * Get the form ID.
   *
   * @return string
   *   The form ID.
   *
   * @see \Drupal\Core\Form\FormInterface::getFormId
   */
  abstract public function getFormId();

  /**
   * Entity buidler for the user and individual profile.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function buildUserAndProfile(array $form, FormStateInterface $form_state) {
    // Try to acquire an existing contact.
    $email = $form_state->getValue(['mail', 0, 'value']);
    $user = $this->acquire($email);

    // If none is found, use parent to create a new user.
    if (!$user) {
      parent::buildEntities($form, $form_state);
    }
    else {
      // Set so that the acquired user will be used.
      $this->user = $user;
    }

    // Ensure we have the indiv role.
    $this->user->addRole('crm_indiv');

    // If the user doesnt have an individual profile, create one.
    if ($this->user->get('profile_crm_indiv')->isEmpty()) {
      /* @var \Drupal\profile\Entity\ProfileInterface $profile */
      $this->profile = $this->entityTypeManager->getStorage('profile')->create([
        'type' => 'crm_indiv',
        'status' => TRUE,
        'is_default' => TRUE,
      ]);
    }
    else {
      $this->profile = $this->user->get('profile_crm_indiv')->entity;
    }

    $should_validate = !$form_state->getTemporaryValue('entity_validated');
    $this->profile->setValidationRequired($should_validate);
    $fields = $this->entityFieldManager->getFieldDefinitions('profile', 'crm_indiv');

    // Populate values on the indiv profile.
    foreach (Element::children($form) as $field_name) {
      // We're only interested in the profile namespace.
      if ('profile' !== ($form[$field_name]['#entity_namespace'] ?? NULL)) {
        continue;
      }

      // Don't overwrite indiv fields.
      if (!$this->profile->get($field_name)->isEmpty()) {
        // Special handling for address which has an incomplete default.
        if ($field_name === 'crm_address') {
          if ($this->profile->get($field_name)->postal_code) {
            continue;
          }
        }
        else {
          continue;
        }
      }

      // Get the widget and extract the form values.
      $widget = $this->getWidget($fields, $field_name);
      $widget->extractFormValues($this->profile->get($field_name), $form, $form_state);
    }
  }

  /**
   * Acquire the contact from email.
   *
   * @param string $email
   *   The email to acquire from.
   *
   * @return \Drupal\decoupled_auth\DecoupledAuthUserInterface|null
   *   The existing contact entity if it exists.
   */
  protected function acquire($email) {
    // If we don't have a contact, look for one.
    $values = [];
    $values['mail'] = $email;

    $context = [
      'name' => $this->getFormId(),
      // Always prefer coupled and allow acquiring protected roles.
      'behavior' => AcquisitionService::BEHAVIOR_PREFER_COUPLED,
    ];

    // Use the global config for first behavior.
    if (\Drupal::config('decoupled_auth.settings')->get('acquisitions.behavior_first')) {
      $context['behavior'] = $context['behavior'] | AcquisitionService::BEHAVIOR_FIRST;
    }

    /* @var \Drupal\decoupled_auth\AcquisitionService $service */
    $service = \Drupal::service('decoupled_auth.acquisition');
    $contact = $service->acquire($values, $context, $acquisition_method);

    return $contact;
  }

  /**
   * Get the fully loaded current user.
   *
   * @return \Drupal\user\UserInterface
   *   The current user.
   */
  protected function currentUserEntity(): UserInterface {
    $current_user = $this->currentUser();
    if (!($current_user instanceof UserInterface)) {
      $current_user = $this->entityTypeManager
        ->getStorage('user')
        ->load($current_user->id());
    }
    return $current_user;
  }

  /**
   * Check whether the current user has the relevant group permissions.
   *
   * @param array $permissions
   *   An array of permissions the user must have for any group.
   *
   * @return bool
   *   Whether they have all the permissions on a group.
   */
  protected function hasGroupPermissions(array $permissions): bool {
    $current_user = $this->currentUserEntity();

    // No organisation means not a dispatcher.
    if ($current_user->get('organisations')->isEmpty()) {
      return FALSE;
    }

    // Get the group membership.
    /** @var \Drupal\group\GroupMembership $membership */
    $membership = $current_user->get('organisations')->first()->membership;

    // Check for the relevant permissions.
    $has_access = !empty($permissions);
    foreach ($permissions as $permission) {
      $has_access = $has_access && $membership->hasPermission($permission);
    }
    return $has_access;
  }

}
