<?php

namespace Drupal\covid_tweaks\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Drupal\user\UserInterface;

/**
 * Form for unassigning users from a request.
 */
class UnassignForm extends ConfirmFormBase {

  /**
   * The node representing the request.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * The account to unassign.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'node_request_unassign';
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('view.support_requests.page_1');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to unassign %user from Request %request?', [
      '%user' => $this->user->label(),
      '%request' => $this->node->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $node = NULL, UserInterface $user = NULL) {
    $this->node = $node;
    $this->user = $user;

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    if ($this->node->get('field_assignees')->isEmpty()) {
      $this->messenger()->addStatus($this->t("Request %request doesn't have any assignees.", [
        '%request' => $this->node->label() . ' [' . $this->node->id() . ']',
        '%user' => $this->user->label() . ' [' . $this->user->id() . ']',
      ]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($this->node->get('field_assignees')->isEmpty()) {
      $this->messenger()->addStatus($this->t("Request %request doesn't have any assignees.", [
        '%request' => $this->node->label() . ' [' . $this->node->id() . ']',
        '%user' => $this->user->label() . ' [' . $this->user->id() . ']',
      ]));
      return;
    }

    $assignees = $this->node->get('field_assignees')->getValue();
    $removed = FALSE;
    foreach ($assignees as $index => $assignee) {
      if ($assignee['target_id'] == $this->user->id()) {
        $removed = TRUE;
        unset($assignees[$index]);
        break;
      }
    }

    if (!$removed) {
      $this->messenger()->addWarning($this->t('User %user is not assigned to Request %request so cannot be unassigned', [
        '%request' => $this->node->label() . ' [' . $this->node->id() . ']',
        '%user' => $this->user->label() . ' [' . $this->user->id() . ']',
      ]));
      return;
    }

    // Update the assignees.
    $this->node->set('field_assignees', $assignees);

    // If we are updating assignees to be empty and the status is assigned, set
    // the status to requested.
    if (empty($assignees) && $this->node->get('field_status')->value == 'assigned') {
      $this->node->set('field_status', 'requested');
    }

    $this->node->save();

    $this->messenger()->addStatus($this->t('User %user has been unassigned from Request %request', [
      '%request' => $this->node->label() . ' [' . $this->node->id() . ']',
      '%user' => $this->user->label() . ' [' . $this->user->id() . ']',
    ]));

    $this->getLogger('covid_tweaks')->notice('User %user has been unassigned from Request %request.', [
      '%request' => $this->node->label() . ' [' . $this->node->id() . ']',
      '%user' => $this->user->label() . ' [' . $this->user->id() . ']',
    ]);

    $form_state->setRedirect('entity.node.canonical', ['node' => $this->node->id()]);
  }

  /**
   * Access callback for unassign form.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The request to unassign someone from.
   * @param \Drupal\user\UserInterface $user
   *   The assignee to unassign.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account trying to access the form.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Whether access should be granted or not.
   */
  public function access(NodeInterface $node, UserInterface $user, AccountInterface $account) {
    // Check whether the user ID to be removed is already assigned.
    $assignees = $node->get('field_assignees')->getValue();
    $removing_assigned = FALSE;
    foreach ($assignees as $assignee) {
      if ($assignee['target_id'] == $user->id()) {
        $removing_assigned = TRUE;
        break;
      }
    }
    // Return whether the current user is allowed to unassign and whether the
    // user being removed is actually assigned.
    return AccessResult::allowedIf($node->access('unassign', $account))
      ->andIf(AccessResult::allowedIf($removing_assigned));
  }

}
