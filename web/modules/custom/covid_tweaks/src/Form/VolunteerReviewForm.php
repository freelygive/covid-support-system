<?php

namespace Drupal\covid_tweaks\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;
use Drupal\user\UserInterface;

/**
 * Form for confirming volunteer assignment for request.
 */
class VolunteerReviewForm extends ConfirmFormBase {

  /**
   * The node representing the request.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * The account to unassign.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'node_request_volunteer_review';
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->node->toUrl();
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to assign %user to Request %request?', [
      '%user' => $this->user->label(),
      '%request' => $this->node->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $node = NULL, UserInterface $user = NULL) {
    $this->node = $node;
    $this->user = $user;

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $not_volunteer = FALSE;
    if ($this->node->get('volunteers')->isEmpty()) {
      $not_volunteer = TRUE;
    }
    else {
      $volunteer_ids = array_map(function ($entity) {
        return $entity->id();
      }, $this->node->get('volunteers')->referencedEntities());
      if (!in_array($this->user->id(), $volunteer_ids)) {
        $not_volunteer = TRUE;
      }
    }

    if ($not_volunteer) {
      $form_state->setErrorByName(NULL, $this->t("%name has not volunteered for this request.", [
        '%name' => $this->user->label(),
      ]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo Do we want to clear the volunteers field or leave it for reference?
    $assignees = $this->node->get('field_assignees')->getValue();
    $assignees[] = ['target_id' => $this->user->id()];
    $this->node->set('field_assignees', $assignees);
    $this->node->set('field_status', 'assigned');
    $this->node->save();

    $this->messenger()->addStatus($this->t('%user has been assigned to request %request', [
      '%request' => $this->node->label(),
      '%user' => $this->user->label(),
    ]));

    $form_state->setRedirect('entity.node.canonical', ['node' => $this->node->id()]);
  }

  /**
   * Access callback for volunteer assign form.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The request to assign someone to.
   * @param \Drupal\user\UserInterface $user
   *   The volunteer to review.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account trying to access the form.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Whether access should be granted or not.
   */
  public function access(NodeInterface $node, UserInterface $user, AccountInterface $account) {
    // Check whether the user ID to be removed is already assigned.
    $volunteer_ids = array_map(function ($entity) {
      return $entity->id();
    }, $this->node->get('volunteers')->referencedEntities());

    $is_volunteer = in_array($user->id(), $volunteer_ids);
    // Return whether the current user is allowed to unassign and whether the
    // user being removed is actually assigned.
    return AccessResult::allowedIf($node->access('review', $account))
      ->andIf(AccessResult::allowedIf($is_volunteer));
  }

}
