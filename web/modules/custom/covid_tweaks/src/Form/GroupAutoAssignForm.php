<?php

namespace Drupal\covid_tweaks\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;
use Drupal\search_api\IndexInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for auto assigning requests to users with Group awareness.
 */
class GroupAutoAssignForm extends AutoAssignForm {

  /**
   * The user we are viewing.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * Group loader service.
   *
   * @var \Drupal\group\GroupMembershipLoaderInterface
   */
  protected $groupLoader;

  /**
   * Group Content storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $groupContentStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $static = parent::create($container);

    $static->groupLoader = $container->get('group.membership_loader');
    $static->groupContentStorage = $container->get('entity_type.manager')->getStorage('group_content');

    return $static;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'node.request.auto-assign.group';
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\user\UserInterface|null $user
   *   The user being views.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   *   Thrown if $user is not provided.
   */
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $user = NULL) {
    $this->user = $user ?? $this->currentUser();
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function buildFilters(array &$form, FormStateInterface $form_state) {
    parent::buildFilters($form, $form_state);

    if ($group_options = $this->getUserGroups($this->user)) {
      $form['filters']['groups'] = [
        '#type' => 'select',
        '#multiple' => TRUE,
        '#title' => $this->t('Groups'),
        '#description' => $this->t('Only show requests and volunteers from specific groups.'),
        '#default_value' => $form_state->getValue('groups'),
        '#options' => $group_options,
        '#weight' => 20,
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getAssignableRequestsQuery(FormStateInterface $form_state) {
    $query = parent::getAssignableRequestsQuery($form_state);

    $filter_group_ids = $form_state->getValue('groups');
    // If the query doesn't specific any groups then filter to all groups the
    // user could have selected.
    if (empty($filter_group_ids)) {
      $filter_group_ids = array_keys($this->getUserGroups($this->user));
    }

    $group_entity_ids = [];
    if ($filter_group_ids) {
      $group_entities = $this->groupContentStorage->loadByProperties([
        'gid' => $filter_group_ids,
        'type' => 'contacts_org-group_node-request',
      ]);

      foreach ($group_entities as $group_entity) {
        $group_entity_ids[] = $group_entity->getEntity()->id();
      }
    }

    // Also include any requests without a group.
    $groupless_requests = $this->getGrouplessRequests();
    $group_entity_ids = array_merge($group_entity_ids, $groupless_requests);

    if (!empty($group_entity_ids)) {
      $query->condition('nid', $group_entity_ids, 'IN');
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  protected function getAssigneeOptionsQuery(NodeInterface $request, IndexInterface $index, int $filter_distance, int $assigned_max, int $nearest_assignees, array &$assign_counts) {
    $query = parent::getAssigneeOptionsQuery($request, $index, $filter_distance, $assigned_max, $nearest_assignees, $assign_counts);

    /** @var \Drupal\group\Entity\GroupContentInterface[] $request_group_content */
    $request_group_content = $this->groupContentStorage->loadByProperties([
      'entity_id' => $request->id(),
      'type' => 'contacts_org-group_node-request',
    ]);

    $group_ids = [];
    foreach ($request_group_content as $request_group_content_item) {
      $group_ids[] = $request_group_content_item->getGroup()->id();
    }

    // If the request doesn't have any groups, use the user's groups.
    if (empty($group_ids)) {
      $group_ids = array_keys($this->getUserGroups($this->user));
    }

    $query->addCondition('group_id', $group_ids, 'IN');

    return $query;
  }

  /**
   * Get the groups for a user belongs to.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The user to get groups for.
   * @param bool $load_groups
   *   Whether to return full group objects or labels.
   *
   * @return array
   *   Group objects or labels as per $load_groups. Keyed by group ID.
   */
  protected function getUserGroups(AccountInterface $user, bool $load_groups = FALSE) {
    $groups = [];

    $current_user_groups = $this->groupLoader->loadByUser($user);
    foreach ($current_user_groups as $current_user_group) {
      if ($load_groups) {
        $groups[$current_user_group->getGroup()->id()] = $current_user_group->getGroup();
      }
      else {
        $groups[$current_user_group->getGroup()->id()] = $current_user_group->getGroup()->label();
      }
    }

    return $groups;
  }

  /**
   * Get the node IDs for any requests without groups.
   *
   * @return int[]
   *   An array of Node IDs for Requests without groups.
   */
  protected function getGrouplessRequests() {
    $query = \Drupal::database()->select('node', 'n');
    $query->fields('n', ['nid']);
    $query->condition('n.type', 'request');
    $query->leftJoin('group_content_field_data', 'gc', 'gc.entity_id = n.nid AND gc.type = :type', [':type' => 'contacts_org-group_node-request']);
    $query->isNull('gc.id');
    return $query->execute()->fetchCol();
  }

}
