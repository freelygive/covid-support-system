<?php

namespace Drupal\covid_tweaks\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeForm;

/**
 * Create request form.
 */
class RequestCreateForm extends NodeForm {

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    unset($actions['preview']);
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $form_state->setRedirect('contacts_user_dashboard.summary', [
      'user' => $this->currentUser()->id(),
    ]);
    $this->messenger()->addMessage('Thank you for your request. It has been received and someone will be in touch shortly.');
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    // Admins can create requests for other people.
    // @todo Maybe move this to a separate staff backend form?
    if (covid_current_user_is_admin()) {
      if ($userid = $this->getRequest()->query->get('user')) {
        /* @var \Drupal\node\Entity\Node $entity */
        $entity = $this->entity;
        $entity->setOwnerId($userid);
      }
    }
    parent::save($form, $form_state);
  }

}
