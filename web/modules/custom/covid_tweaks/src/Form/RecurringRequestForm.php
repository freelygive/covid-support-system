<?php

namespace Drupal\covid_tweaks\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Recurring request edit forms.
 *
 * @ingroup covid_tweaks
 */
class RecurringRequestForm extends ContentEntityForm {

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->account = $container->get('current_user');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var \Drupal\covid_tweaks\Entity\RecurringRequest $entity */
    $form = parent::buildForm($form, $form_state);

    // Staff can set users based on user ID in query string.
    // Incoming links from contacts dashboard have this.
    if ($this->getOperation() == 'add' && covid_current_user_is_admin()) {
      if ($user_id = $this->getRequest()->query->get('user')) {
        $form['user_id']['widget'][0]['target_id']['#default_value'] = User::load($user_id);
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /* @var \Drupal\covid_tweaks\Entity\RecurringRequest $entity */
    $entity = $this->entity;

    // Immediately create the next request. Subsequent ones will be created
    // on cron.
    if ($request = $entity->createNextRequestFromSchedule()) {
      $request->save();
    }

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the Recurring request.'));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the Recurring request.'));
    }
    $form_state->setRedirect('entity.recurring_request.canonical', ['recurring_request' => $entity->id()]);
  }

}
