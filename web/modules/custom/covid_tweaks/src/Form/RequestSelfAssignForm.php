<?php

namespace Drupal\covid_tweaks\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides a confirmation form for self assigning to a request.
 */
class RequestSelfAssignForm extends ContentEntityConfirmFormBase {

  /**
   * The user being cancelled.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you would be able to do the support request %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->entity->toUrl();
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    $description = new TranslatableMarkup('Once you have volunteered for this request your offer will be reviewed before it is accepted.');
    return $description;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Volunteer');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#description' => $this->t('Please give some information about yourself to be passed on the requester.'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $entity = parent::validateForm($form, $form_state);

    $volunteers = [];
    foreach ($entity->get('volunteers') as $volunteer) {
      $target_id = $volunteer->target_id;
      $volunteers[$target_id] = $target_id;
    }

    // Make sure people cant repeatedly volunteer for the same request.
    if (in_array($this->currentUser()->id(), $volunteers)) {
      $form_state->setErrorByName('volunteers', 'You have already volunteered for this request.');
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Progress status if this is the first volunteer.
    $status = $this->entity->get('field_status')->value;
    if ($status == 'requested') {
      $this->entity->set('field_status', 'pending');
    }
    /** @var \Drupal\Core\Field\EntityReferenceFieldItemList $volunteers */
    $volunteers = $this->entity->get('volunteers');
    $volunteers->appendItem([
      'target_id' => $this->currentUser()->id(),
      'message' => $form_state->getValue('message'),
    ]);
    $this->entity->save();
    $this->messenger()->addStatus($this->t('Your offer to volunteer has been logged.'));
    $volunteer = $this->currentUser();
    $this->logger('covid_tweaks')->notice('The request %title (%id) has been volunteered for by %name.', [
      '%name' => $volunteer->getDisplayName(),
      '%title' => $this->entity->label(),
      '%id' => $this->entity->id(),
    ]);

    $route_params = ['node' => $this->entity->id()];
    $form_state->setRedirect('entity.node.canonical', $route_params);
  }

}
