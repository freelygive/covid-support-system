<?php

namespace Drupal\covid_tweaks\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a confirmation form for cancelling a request.
 */
class RequestCancelForm extends ContentEntityConfirmFormBase {

  /**
   * The user being cancelled.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to cancel the request %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    if (covid_current_user_is_admin()) {
      // Send admins back to the request list page.
      return new Url('view.support_requests.page_1');
    }
    // Regular users back to their dashboard.
    return new Url('contacts_user_dashboard.summary', [
      'user' => $this->currentUser()->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    $description = 'Once this request has been cancelled it will no longer appear in the request directory.';
    return $description;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Cancel request');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->set('status', FALSE);
    $this->entity->set('field_status', 'cancelled');
    $this->entity->save();

    $this->messenger()->addStatus($this->t('Your request has been cancelled.'));
    $this->logger('covid_tweaks')->notice('A request has been cancelled by %name.', ['%name' => $this->entity->getOwner()->label()]);

    $form_state->setRedirect(
      'entity.user.canonical',
      ['user' => $this->entity->getOwner()->id()]
    );

  }

}
