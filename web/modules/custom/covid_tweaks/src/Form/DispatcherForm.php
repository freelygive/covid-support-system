<?php

namespace Drupal\covid_tweaks\Form;

use Drupal\contacts\Form\AddContactBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Dispatcher form.
 */
class DispatcherForm extends AddContactBase {

  use ContactFormTrait;

  /**
   * The group membership.
   *
   * @var \Drupal\group\Entity\GroupContentInterface|null
   */
  protected $groupContent;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'covid_tweaks_dispatcher_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    // Get the profile entity.
    /** @var \Drupal\profile\Entity\ProfileInterface $profile */
    $profile = $this->entityTypeManager->getStorage('profile')->create(['type' => 'crm_indiv']);
    $profile->setOwnerId($this->currentUser()->id());
    $profile_fields = $this->entityFieldManager->getFieldDefinitions('profile', 'crm_indiv');
    $profile_fields['crm_name']->setRequired(TRUE);

    // Create the group content.
    $group_content = $this->entityTypeManager->getStorage('group_content')->create([
      'type' => 'contacts_org-group_membership',
      'group_roles' => ['contacts_org-dispatcher'],
    ]);
    $group_content_fields = $this->entityFieldManager->getFieldDefinitions('group_content', 'contacts_org-group_membership');

    $fields = [
      // Profile: Name.
      'crm_name' => [
        'entity' => 'profile',
        'weight' => 0,
      ],
      // User: Email.
      // @see: \Drupal\contacts\Form\AddContactBase::buildForm
      // Profile: Phone.
      'crm_phone' => [
        'entity' => 'profile',
        'weight' => $weight = 2,
      ],
      // Profile: Address.
      'crm_address' => [
        'entity' => 'profile',
        'weight' => ++$weight,
      ],
      'group_roles' => [
        'entity' => 'group_content',
        'weight' => ++$weight,
        'config' => ['type' => 'options_buttons'],
      ],
    ];

    foreach ($fields as $field_name => $field_info) {
      $entity = ${$field_info['entity']};
      $field_definitions = ${$field_info['entity'] . '_fields'};
      $form[$field_name] = $this->getWidgetForm($entity, $field_definitions, $field_name, $form, $form_state, $field_info['config'] ?? []);
      $form[$field_name]['#weight'] = $field_info['weight'];
      $form[$field_name]['#entity_namespace'] = $field_info['entity'];
    }

    // Make adjustments beyond widget config.
    $form['crm_name']['widget'][0]['#theme_wrappers'] = [];
    unset($form['crm_address']['widget'][0]['#type']);
    unset($form['mail']['widget'][0]['value']['#description']);
    $form['group_roles']['widget']['#pre_render'] = [];

    // We only want to show the checkbox if we have submitted the form and the
    // email acquires an existing user.
    $input = $form_state->getUserInput();
    if (!empty($input['mail'][0]['value'])) {
      $email = $input['mail'][0]['value'];
      $contact = $this->acquire($email);
      if (!empty($contact) && $contact->organisations->isEmpty()) {
        $form['confirm_acquisition'] = [
          '#type' => 'checkbox',
          '#title' => new TranslatableMarkup('Please use the contact details on file for the above entered email address.'),
          '#weight' => 99,
        ];
      }
    }

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildEntities(array $form, FormStateInterface $form_state) {
    // Let the trait build the user and profile.
    $this->buildUserAndProfile($form, $form_state);

    // Ensure we have the indiv role.
    $this->user->addRole('dispatcher');

    $current_user = $this->currentUserEntity();

    // Create the group content.
    $this->groupContent = $this->entityTypeManager
      ->getStorage('group_content')->create([
        'type' => 'contacts_org-group_membership',
        'group_roles' => ['contacts_org-dispatcher'],
        'gid' => $current_user->get('organisations')->first()->entity->gid,
      ]);
    $this->groupContent->setValidationRequired(!$form_state->getTemporaryValue('entity_validated'));
    $fields = $this->entityFieldManager->getFieldDefinitions('group_content', 'contacts_org-group_membership');

    // Populate values on the group content.
    foreach (Element::children($form) as $field_name) {
      // We're only interested in the request namespace.
      if ('group_content' !== ($form[$field_name]['#entity_namespace'] ?? NULL)) {
        continue;
      }

      $widget = $this->getWidget($fields, $field_name);
      $widget->extractFormValues($this->groupContent->get($field_name), $form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Additional validation if the user already exists.
    if (!$this->user->isNew()) {
      // Show an error if the user already has a group.
      if ($this->user->organisations->count()) {
        $form_state->setErrorByName('', $this->t('A contact with this email already exists in the system and is already part of an organisation. Being part of multiple organisations is not currently supported.'));
      }
      // Otherwise show a validation error if the user has not checked the
      // confirmation checkbox.
      elseif (!$form_state->getValue('confirm_acquisition')) {
        // Check fields.
        $message = $this->t("A contact with this email already exists in the system.<br>
          For data protection reasons, we will only use the contact details that we already have on file for you.<br>
          To change your details, simply log in or register an account with the same email and update your information.<br>
          <strong>To continue please select the checkbox to confirm you have read this and submit the form.</strong>");
        $form_state->setErrorByName('', $message);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->groupContent->set('entity_id', $this->user->id());
    $this->groupContent->save();

    $form_state->setRedirect('view.organisation_offers.dispatchers', [
      'user' => $this->currentUser()->id(),
    ]);
    $this->messenger()->addMessage('Your dispatcher has been added.');
  }

}
