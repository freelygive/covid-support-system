<?php

namespace Drupal\covid_tweaks;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\covid_tweaks\Form\SettingsForm;
use Drupal\user\UserInterface;

/**
 * Privacy service.
 */
class Privacy {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The privacy config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a Privacy object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory, AccountInterface $current_user, TranslationInterface $string_translation) {
    $this->entityTypeManager = $entity_type_manager;
    $this->config = $config_factory->get('covid_tweaks.settings');
    $this->currentUser = $current_user;
    $this->setStringTranslation($string_translation);
  }

  /**
   * Get the privacy aware display name.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The user we want the display name for.
   *
   * @return array
   *   The display name in a render array with cacheability metadata.
   */
  public function getDisplayName(AccountInterface $user): array {
    $title = [
      '#markup' => $user->getDisplayName(),
      '#allowed_tags' => Xss::getHtmlTagList(),
      '#cache' => [
        'tags' => ['user:' . $user->id()],
      ],
    ];

    // In order of most cacheable, non indivs are not restricted.
    if (!in_array('crm_indiv', $user->getRoles(TRUE))) {
      return $title;
    }

    // Those with contact access can view all.
    $title['#cache']['contexts'][] = 'user.permissions';
    if ($this->currentUser->hasPermission('view contacts')) {
      return $title;
    }

    // A user can see their own.
    $title['#cache']['contexts'][] = 'user';
    if ($user->id() === $this->currentUser->id()) {
      return $title;
    }

    if (!$user instanceof UserInterface) {
      $user = $this->entityTypeManager->getStorage('user')->load($user);
    }

    // Dispatchers can see their own volunteers.
    $title['#cache']['tags'][] = 'group_content_list';
    if ($this->isDispatcherForUser($user, $this->currentUser)) {
      return $title;
    }

    // Name now needs to be restricted down to either just given or completely
    // hidden. Ensure we have a full user account so we can get the name field.
    /** @var \Drupal\profile\Entity\ProfileInterface $profile */
    $profile = $user->get('profile_crm_indiv')->entity;
    $name = $profile && $profile->hasField('crm_name') ?
      $profile->get('crm_name')->given : NULL;

    // If there is a name, check whether the name is accessible.
    $cacheability = CacheableMetadata::createFromRenderArray($title);
    if ($name && !$this->restrictName($user, $cacheability)) {
      $title['#markup'] = $name;
    }
    // Otherwise, restrict it.
    else {
      $title['#markup'] = $this->t('User #@id', ['@id' => $user->id()]);
    }
    $cacheability->applyTo($title);

    return $title;
  }

  /**
   * Whether we should restrict the name.
   *
   * @param \Drupal\Core\Session\AccountInterface|null $user
   *   Optionally a specific user, otherwise only checks the general config.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface|null $cacheability
   *   Optionally, cacheability metadata to refine.
   *
   * @return bool
   *   Whether the name should be restricted.
   */
  public function restrictName(?AccountInterface $user = NULL, ?RefinableCacheableDependencyInterface $cacheability = NULL): bool {
    // If names are public, things are simple.
    $cacheability->addCacheableDependency($this->config);
    if ($this->config->get('privacy.names') === SettingsForm::PRIVACY_NAMES_PUBLIC) {
      return FALSE;
    }

    // If names are private and we have a specific user, assigned volunteers
    // should have access.
    if ($user) {
      $cacheability->addCacheTags(['node_list']);
      return !$this->hasActiveAssignedRequestForUser($user);
    }

    // Otherwise it should be restricted.
    return TRUE;
  }

  /**
   * Check the current user has any active assigned requests for a given user.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The user the requests belong to.
   *
   * @return bool
   *   Whether the current user has any active assigned requests.
   */
  public function hasActiveAssignedRequestForUser(AccountInterface $user): bool {
    $query = $this->entityTypeManager
      ->getStorage('node')
      ->getQuery()
      ->count()
      ->accessCheck(FALSE);

    $query->condition('type', 'request');
    $query->condition('field_assignees', $this->currentUser->id());
    $query->condition('field_status', 'assigned');

    return $query->execute() > 0;
  }

  /**
   * Check whether the current user is a dispatcher for the given user.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user to check against.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account of the dispatcher.
   *
   * @return bool
   *   Whether the current user is a dispatcher for the user.
   */
  public function isDispatcherForUser(UserInterface $user, AccountInterface $account): bool {
    // Get the groups the account is a dispatcher for.
    if (!($account instanceof UserInterface)) {
      $account = $this->entityTypeManager
        ->getStorage('user')
        ->load($account->id());
    }

    $org_ids = [];
    foreach ($account->get('organisations') as $item) {
      /** @var \Drupal\group\GroupMembership $membership */
      $membership = $item->membership;
      if ($membership->hasPermission('view group_membership content')) {
        $org_ids[$membership->getGroup()->id()] = $item->entity;
      }
    }

    if (empty($org_ids)) {
      return FALSE;
    }

    foreach ($user->get('organisations') as $item) {
      $org_id = $item->entity->gid->target_id;
      if (isset($org_ids[$org_id])) {
        return TRUE;
      }
    }

    return FALSE;
  }

}
