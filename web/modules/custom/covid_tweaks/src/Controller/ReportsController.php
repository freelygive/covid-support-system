<?php

namespace Drupal\covid_tweaks\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\covid_tweaks\Statistics;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Covid Tweaks routes.
 */
class ReportsController extends ControllerBase {

  /**
   * The statistics service.
   *
   * @var \Drupal\covid_tweaks\Statistics
   */
  protected $statistics;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\covid_tweaks\Statistics $statistics
   *   The statistics service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Statistics $statistics, EntityFieldManagerInterface $entity_field_manager, TranslationInterface $string_translation, DateFormatterInterface $date_formatter) {
    $this->entityTypeManager = $entity_type_manager;
    $this->statistics = $statistics;
    $this->entityFieldManager = $entity_field_manager;
    $this->setStringTranslation($string_translation);
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('covid_tweaks.statistics'),
      $container->get('entity_field.manager'),
      $container->get('string_translation'),
      $container->get('date.formatter'),
    );
  }

  /**
   * Builds the response.
   */
  public function volunteers() {
    $build['summary'] = $this->buildVolunteerSummary();

    $build['verified_roles'] = [
      '#type' => 'view',
      '#name' => 'covid_reports_verified_roles',
      '#display_id' => 'pie_chart',
    ];

    $build['volunteer_numbers'] = $this->buildVolunteerPlot('volunteer_source', 'Volunteer sign-up');

    $build['volunteers_by_source'] = $this->buildVolunteerTable('volunteer_source', $this->t('Volunteers by source of application form'));
    $build['volunteers_by_referred'] = $this->buildVolunteerTable('volunteer_referred_to', $this->t('Volunteers by organisations they are referred to'));
    $build['volunteers_by_role'] = $this->buildVolunteerTable('volunteer_role_first', $this->t('Volunteers by skills'));

    return $build;
  }

  /**
   * Build the volunteer summary render array.
   *
   * @return array
   *   The summary render array.
   */
  protected function buildVolunteerSummary(): array {
    $summary_stats = $this->statistics->getSummaryStats();
    $build = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => [
          'statistics',
          'row',
          'justify-content-around',
          'mb-4',
        ],
      ],
    ];
    $build['offers'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => [
          'statistic',
          'statistic__requests',
          'col-12',
          'col-sm-5',
          'col-md-3',
          'my-2',
        ],
      ],
      'title' => [
        '#type' => 'html_tag',
        '#tag' => 'h3',
        '#attributes' => [
          'class' => ['statistic--title'],
          'style' => 'min-height:5vw',
        ],
        '#value' => $this->t('Sign-ups'),
      ],
      'number' => [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#attributes' => ['class' => ['statistic--number']],
        '#value' => $summary_stats['offers'],
      ],
    ];
    $build['volunteers'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => [
          'statistic',
          'statistic__offers',
          'col-12',
          'col-sm-5',
          'col-md-3',
          'my-2',
        ],
      ],
      'title' => [
        '#type' => 'html_tag',
        '#tag' => 'h3',
        '#attributes' => [
          'class' => ['statistic--title'],
          'style' => 'min-height:5vw',
        ],
        '#value' => $this->t('Approved volunteers'),
      ],
      'number' => [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#attributes' => ['class' => ['statistic--number']],
        '#value' => $summary_stats['volunteers'],
      ],
    ];

    $build['processing_time'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => [
          'statistic',
          'statistic__processing',
          'col-12',
          'col-sm-5',
          'col-md-4',
          'col-lg-3',
          'my-2',
        ],
      ],
      '#prefix' => '<div class="w-100 d-lg-none"></div>',
      'title' => [
        '#type' => 'html_tag',
        '#tag' => 'h3',
        '#attributes' => [
          'class' => ['statistic--title'],
          'style' => 'min-height:5vw',
        ],
        '#value' => $this->t('Processing time'),
      ],
      'number' => [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#attributes' => ['class' => ['statistic--number']],
      ],
    ];
    $processing_time = $this->statistics->getVolunteerProcessingTime();
    if ($processing_time) {
      [$number, $unit] = explode(' ', $this->dateFormatter->formatInterval($processing_time, 1));
      $build['processing_time']['number']['#value'] = $number;
      $build['processing_time']['title']['#value'] = $this->t('Processing @unit', [
        '@unit' => $unit,
      ]);
    }
    else {
      $build['processing_time']['number']['#value'] = $this->t('N/A');
    }
    return $build;
  }

  /**
   * Build the volunteer details table.
   *
   * @return array
   *   The render array.
   */
  protected function buildVolunteerPlot($group, $title): array {
    // Build the chart structure.
    $chart = [
      'chart' => [
        'type' => 'column',
      ],
      'title' => ['text' => $title],
      'xAxis' => [['labels' => []]],
      'yAxis' => [
        [
          'labels' => [
            'prefix' => '',
            'suffix' => '',
          ],
          'stackLabels' => ['enabled' => TRUE],
          'allowDecimals' => FALSE,
          'title' => ['text' => $this->t('Volunteers')],
        ],
      ],
      'legend' => [
        'align' => 'right',
        'verticalAlign' => 'top',
        'floating' => TRUE,
        'borderColor' => '#CCC',
        'borderWidth' => 1,
      ],
      'tooltip' => [
        'headerFormat' => '<b>{point.x}</b><br/>',
        'pointFormat' => '{series.name}: {point.y}<br/>Total: {point.stackTotal}',
      ],
      'plotOptions' => [
        'column' => [
          'stacking' => 'normal',
        ],
      ],
      'series' => [
        'submitted' => [
          'name' => 'Signed up',
          'data' => [],
        ],
        'onboarded' => [
          'name' => 'Referred',
          'data' => [],
        ],
      ],
      'exporting' => ['enabled' => FALSE],
    ];

    // Fill out the data.
    foreach ($this->statistics->getVolunteerProgress($group) as $week_key => $week) {
      $chart['xAxis'][0]['labels'][$week['date']->format('j M')] = $week['date']->format('j M');
      $chart['xAxis'][0]['categories'][] = $week['date']->format('j M');
      foreach ($chart['series'] as $row_key => &$series) {
        $series['data'][] = $week[$row_key] ?? 0;
      }
    }
    $chart['series'] = array_values($chart['series']);

    // Build the render array for the chart.
    return [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => ['charts-highchart', 'my-4'],
        'data-chart' => json_encode($chart),
        'style' => 'min-height:400px;height:60vh;max-height:900px',
      ],
      '#attached' => [
        'library' => [
          'charts_highcharts/highcharts',
        ],
      ],
    ];
  }

  /**
   * Build the volunteer details table.
   *
   * @param string $group
   *   The field to group by.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $title
   *   The title for the table.
   *
   * @return array
   *   The render array.
   */
  protected function buildVolunteerTable(string $group, TranslatableMarkup $title): array {
    // Build the table structure.
    $table = [
      '#type' => 'table',
      '#header' => [
        'key' => $this->t('Week'),
      ],
      '#rows' => [],
    ];

    // Get the possible values for the group field to create the rows.
    $total_options = [
      'submitted' => 'Signed up',
      'onboarded' => 'Referred',
    ];
    $profile = $this->entityTypeManager()
      ->getStorage('profile')
      ->create(['type' => 'volunteer']);
    $group_options = $this->entityFieldManager
      ->getFieldStorageDefinitions('profile')[$group]
      ->getOptionsProvider($group, $profile)
      ->getPossibleOptions();

    foreach ($total_options as $total_key => $total_label) {
      $table['#rows'][$total_key] = [
        'key' => [
          'header' => TRUE,
          'data' => ['#markup' => $total_label],
        ],
      ];

      foreach ($group_options as $group_key => $group_label) {
        $table['#rows']["{$total_key}:{$group_key}"] = [
          'key' => [
            'data' => ['#markup' => $group_label],
            'style' => 'padding-left:2rem',
          ],
        ];
      }
    }

    // Fill out the data.
    foreach ($this->statistics->getVolunteerProgress($group) as $week_key => $week) {
      $table['#header'][$week_key] = $week['date']->format('j M');
      foreach ($table['#rows'] as $row_key => &$row) {
        if (isset($week[$row_key])) {
          $row[$week_key] = [
            'header' => $row['key']['header'] ?? FALSE,
            'data' => ['#markup' => $week[$row_key]],
          ];
        }
      }
    }
    unset($row);

    // Remove any empty rows.
    foreach ($table['#rows'] as $row_key => $row) {
      if (strpos($row_key, ':') === FALSE) {
        continue;
      }

      $has_value = FALSE;
      foreach ($row as $col => $cell) {
        if ($col === 'key') {
          continue;
        }
        if (!empty($cell['data']['#markup'])) {
          $has_value = TRUE;
          break;
        }
      }

      if (!$has_value) {
        unset($table['#rows'][$row_key]);
      }
    }

    return [
      'header' => [
        '#type' => 'html_tag',
        '#tag' => 'h3',
        '#value' => $title,
        '#attributes' => [
          'class' => ['pt-5'],
        ],
      ],
      'table' => $table,
    ];
  }

  /**
   * Build the volunteer flow sankey chart.
   *
   * @return array
   *   The chart render array.
   */
  protected function buildVolunteerFlow() {
    $chart = [
      'title' => ['text' => $this->t('Volunteer flow')],
      'series' => [
        [
          'data' => [],
          'type' => 'sankey',
          'name' => $this->t('Volunteer flow'),
        ],
      ],
      'chart' => [
        'type' => 'sankey',
      ],
      'xAxis' => [['labels' => []]],
      'yAxis' => [['labels' => []]],
      'exporting' => ['enabled' => FALSE],
    ];

    // Get our raw data.
    $query = $this->statistics->getVolunteerBaseQuery();
    $query->exists('volunteer_status');
    $query->groupBy('volunteer_source');
    $query->groupBy('volunteer_status');
    $query->groupBy('volunteer_referred_to');
    $data = $query->execute();

    // Process the data into our series.
    foreach ($data as $row) {
      $is_inactive = in_array($row['volunteer_status_value'], [
        'widthdrawn',
        'no_contact',
      ]);

      // Every row has a source -> status.
      $data = [
        'from' => $row['volunteer_source_value'] ? "source:{$row['volunteer_source_value']}" : $this->t('Unknown'),
        'to' => "status:{$row['volunteer_status_value']}",
        'weight' => (int) $row['profile_id_count'],
        // If the status is an end of the road, indicate as such.
        'outgoing' => $is_inactive,
      ];
      $key = "source_status:{$data['from']}:{$data['to']}";
      if (isset($chart['series'][0]['data'][$key])) {
        $chart['series'][0]['data'][$key]['weight'] += $data['weight'];
      }
      else {
        $chart['series'][0]['data'][$key] = $data;
      }

      // Active referred rows have a status -> referred.
      if ($row['volunteer_referred_to_value'] && !$is_inactive) {
        $data = [
          'from' => "status:{$row['volunteer_status_value']}",
          'to' => "referred_to:{$row['volunteer_referred_to_value']}",
          'weight' => (int) $row['profile_id_count'],
        ];
        $key = "status_referred:{$data['from']}:{$data['to']}";
        if (isset($chart['series'][0]['data'][$key])) {
          $chart['series'][0]['data'][$key]['weight'] += $data['weight'];
        }
        else {
          $chart['series'][0]['data'][$key] = $data;
        }
      }
    }
    $chart['series'][0]['data'] = array_values($chart['series'][0]['data']);

    // Build our node labels.
    $storage_definitions = $this->entityFieldManager->getFieldStorageDefinitions('profile');
    foreach ($storage_definitions['volunteer_source']->getSetting('allowed_values') as $value => $label) {
      $chart['series'][0]['nodes'][] = [
        'id' => "source:{$value}",
        'name' => $label,
      ];
    }
    foreach ($storage_definitions['volunteer_status']->getSetting('allowed_values') as $value => $label) {
      $chart['series'][0]['nodes'][] = [
        'id' => "status:{$value}",
        'name' => $label,
      ];
    }
    foreach ($storage_definitions['volunteer_referred_to']->getSetting('allowed_values') as $value => $label) {
      $chart['series'][0]['nodes'][] = [
        'id' => "referred_to:{$value}",
        'name' => $label,
      ];
    }

    // Build the render array for the chart.
    return [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => ['charts-highchart', 'my-4'],
        'data-chart' => json_encode($chart),
        'style' => 'min-height:500px;height:80vh;max-height:900px',
      ],
      '#attached' => [
        'library' => [
          "charts_highcharts/highcharts",
          'covid_tweaks/highcharts_sankey',
        ],
      ],
    ];
  }

}
