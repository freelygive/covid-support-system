<?php

namespace Drupal\covid_tweaks\Controller;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Drupal\covid_tweaks\Statistics;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Controller for covid system level routes.
 */
class SystemController extends ControllerBase {

  /**
   * The statistics service.
   *
   * @var \Drupal\covid_tweaks\Statistics
   */
  protected $statistics;

  /**
   * SystemController constructor.
   *
   * @param \Drupal\covid_tweaks\Statistics $statistics
   *   The statistics service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(Statistics $statistics, TranslationInterface $string_translation) {
    $this->statistics = $statistics;
    $this->setStringTranslation($string_translation);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('covid_tweaks.statistics'),
      $container->get('string_translation'),
    );
  }

  /**
   * The COVID landing page.
   *
   * @return array
   *   The page render array.
   *
   * @todo: Make this configurable, possibly make this all a layout?
   */
  public function landing(): array {
    $content = [];

    $content['map'] = [
      '#type' => 'view',
      '#name' => 'contact_request_directory',
      '#display_id' => 'block_1',
      '#attributes' => [
        'class' => ['container-breakout'],
      ],
    ];

    $content['intro'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => ['text-center', 'px-5'],
      ],
      'title' => [
        '#type' => 'html_tag',
        '#tag' => 'h2',
        '#value' => $this->t('What is Mutual Aid?'),
        '#attributes' => [
          'class' => ['my-4'],
        ],
      ],
      [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => 'Sometimes institutions fail and the community at large has to step in and make sure that its members are able to access life-saving resources, such as food, water, medical supplies, medication, etc.',
      ],
      [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => 'Mutual aid is not charity: rather than creating a centralized organization where one person is exchanging resources with someone else. Mutual aid creates a symbiotic relationship and builds community, where all people offer material goods or assistance to one another. Mutual aid organizing is volunteer-run, transparent, and driven by the needs articulated by community members.',
      ],
    ];

    $content['stats'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => [
          'statistics',
          'row',
          'my-5',
        ],
      ],
    ];

    $stats = $this->statistics->getSummaryStats();
    $content['stats']['requests'] = $this->getStatRender('Requests', $stats['requests'], 'requests');
    $content['stats']['offers'] = $this->getStatRender('Offers', $stats['offers'], 'offers');
    $content['stats']['connections'] = $this->getStatRender('Connections', $stats['connections'], 'connections');

    $text = $this->t('Search requests');
    $icon = new FormattableMarkup(
      '<svg data-layer="45dd2511-c3d5-4bbe-9c9e-1688498756db" preserveAspectRatio="none" viewBox="0 0 78.0540771484375 79.123779296875" style="margin-top:-18%"><title>@text</title></title><path d="M 76.99256134033203 68.40756988525391 L 61.79224395751953 53.00157165527344 C 61.10617065429688 52.30621719360352 60.17616653442383 51.91990661621094 59.20041656494141 51.91990661621094 L 56.71531295776367 51.91990661621094 C 60.92322158813477 46.46522903442383 63.42357635498047 39.6043815612793 63.42357635498047 32.14089584350586 C 63.42357635498047 14.38614177703857 49.22949981689453 0 31.71178817749023 0 C 14.19407272338867 0 0 14.38614177703857 0 32.14089584350586 C 0 49.89565277099609 14.19407367706299 64.28179168701172 31.71178817749023 64.28179168701172 C 39.07563018798828 64.28179168701172 45.84487533569336 61.74760055541992 51.22673416137695 57.48275375366211 L 51.22673416137695 60.00148773193359 C 51.22673416137695 60.99044036865234 51.60788345336914 61.93303298950195 52.2939567565918 62.62838745117188 L 67.49427032470703 78.03438568115234 C 68.92739868164062 79.48690795898438 71.24480438232422 79.48690795898438 72.66268157958984 78.03438568115234 L 76.97731781005859 73.66136932373047 C 78.41044616699219 72.20883941650391 78.41044616699219 69.86008453369141 76.99256134033203 68.40756225585938 Z M 31.71178817749023 51.91990661621094 C 20.93282699584961 51.91990661621094 12.1968412399292 43.08115768432617 12.1968412399292 32.14089584350586 C 12.1968412399292 21.2160816192627 20.91758346557617 12.36188316345215 31.71178817749023 12.36188316345215 C 42.49074554443359 12.36188316345215 51.22673416137695 21.20063018798828 51.22673416137695 32.14089584350586 C 51.22673416137695 43.06571197509766 42.50598907470703 51.91990661621094 31.71178817749023 51.91990661621094 Z"></path></svg>',
      [
        '@text' => $text,
      ],
    );
    $link = Link::createFromRoute(
      $icon,
      'view.contact_request_directory.page_1',
      [],
      [
        'attributes' => [
          'class' => ['my-4', 'btn', 'btn-primary', 'btn-lg', 'btn-icon'],
          'title' => $text,
        ],
      ],
    );
    $content['search'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => ['text-center', 'px-5', 'pb-4'],
      ],
      'title' => [
        '#type' => 'html_tag',
        '#tag' => 'h2',
        '#value' => $this->t('Search Requests'),
        '#attributes' => [
          'class' => ['my-4'],
        ],
      ],
      [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => 'Can you help your community? You want to make an exchange for goods or services? Here you can search through the list and find all the requests made by your neighbors and place an offer. Don’t find a request that matches your offer? Place and offer and the platform algorithm will match you when a request comes up.',
      ],
      'link' => $link->toRenderable(),
    ];
    $content['search']['link']['#weight'] = 99;

    return $content;
  }

  /**
   * Build the render array for a statistic.
   *
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|string $title
   *   The title.
   * @param mixed $value
   *   The value to show.
   * @param string $machine_name
   *   Name of the stat to use in class attribute.
   *
   * @return array
   *   The render array.
   */
  protected function getStatRender($title, $value, $machine_name) {
    return [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => [
          'statistic',
          'statistic__' . $machine_name,
          'col-12',
          'col-md-4',
        ],
      ],
      'title' => [
        '#type' => 'html_tag',
        '#tag' => 'h3',
        '#attributes' => ['class' => 'statistic--title'],
        '#value' => $title,
      ],
      'number' => [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#attributes' => ['class' => 'statistic--number'],
        '#value' => $value,
      ],
    ];
  }

  /**
   * Custom access denied handler.
   *
   * @return array
   *   The render array.
   */
  public function denied() {
    return [
      '#markup' => '<p>You are trying to access a page that you do not have permission to view.<br>If you have just created an account, you may need to wait for an administrator to review your details and grant you elevated privileges in order to use this service.</p>',
    ];
  }

  /**
   * Volunteer profile redirect.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect.
   */
  public function volunteerProfile() {
    $url = Url::fromRoute('profile.user_page.single', [
      'user' => $this->currentUser()->id(),
      'profile_type' => 'volunteer',
    ]);
    return new RedirectResponse($url->toString());
  }

}
