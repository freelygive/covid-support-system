<?php

namespace Drupal\covid_tweaks\Controller;

use Drupal\contacts_user_dashboard\Controller\UserDashboardProfileController;
use Drupal\Core\Link;
use Drupal\profile\Entity\ProfileTypeInterface;
use Drupal\user\UserInterface;

/**
 * Controller for Profile form.
 *
 * Hides volunteer form if user isn't a volunteer.
 *
 * @package Drupal\covid_tweaks\Controller
 */
class ProfileController extends UserDashboardProfileController {

  /**
   * {@inheritdoc}
   */
  public function singlePage(UserInterface $user, ProfileTypeInterface $profile_type) {
    // Only allow volunteer profile form access if a profile already exists or
    // the user has said they are a volunteer on their individual profile.
    if ($profile_type->id() == 'volunteer') {
      $profile_storage = $this->entityTypeManager()->getStorage('profile');
      $indiv_profile = $profile_storage->loadByUser($user, 'crm_indiv');

      // Check if the user has said they're a volunteer.
      $is_volunteer = FALSE;
      if ($indiv_profile && !$indiv_profile->get('field_are_you_looking_to_offer_o')->isEmpty()) {
        foreach ($indiv_profile->get('field_are_you_looking_to_offer_o')->getValue() as $item) {
          if ($item['value'] == 'volunteer') {
            $is_volunteer = TRUE;
            break;
          }
        }
      }

      // Show the message if the user is not a volunteer.
      if (!$is_volunteer) {
        $output = [];

        $output['info'] = [
          '#markup' => $this->t('<p>You have not yet made an offer of help. To volunteer please complete the %link form.</p>',
            ['%link' => Link::createFromRoute('Offer Help', 'covid_tweaks.offer.public')->toString()]
          ),
        ];
        return $output;
      }
    }

    return parent::singlePage($user, $profile_type);
  }

}
