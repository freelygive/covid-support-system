<?php

namespace Drupal\covid_tweaks\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\covid_tweaks\Form\UnassignForm;
use Drupal\covid_tweaks\Form\VolunteerReviewForm;
use Drupal\node\Controller\NodeController;
use Drupal\node\NodeInterface;
use Drupal\node\NodeTypeInterface;
use Drupal\user\UserInterface;

/**
 * Controller for Request routes.
 */
class RequestController extends NodeController {

  /**
   * Builds the add form.
   *
   * @param \Drupal\node\NodeTypeInterface $node_type
   *   The node type.
   *
   * @return array
   *   The render array.
   */
  public function add(NodeTypeInterface $node_type) {
    if ($node_type->id() == 'request') {
      $node = $this->entityTypeManager()->getStorage('node')->create([
        'type' => $node_type->id(),
      ]);

      $form = $this->entityFormBuilder()->getForm($node, 'create');

      return $form;
    }
    else {
      return parent::add($node_type);
    }
  }

  /**
   * Builds the add form for staff requests.
   *
   * @param \Drupal\node\NodeTypeInterface $node_type
   *   The node type.
   * @param \Drupal\user\UserInterface $user
   *   The user the request is for.
   *
   * @return array
   *   The render array
   */
  public function staffAdd(NodeTypeInterface $node_type, UserInterface $user) {
    if ($node_type->id() == 'request') {
      $node = $this->entityTypeManager()->getStorage('node')->create([
        'type' => $node_type->id(),
        'uid' => $user->id(),
      ]);

      $form = $this->entityFormBuilder()->getForm($node, 'create');

      return $form;
    }
    else {
      return parent::add($node_type);
    }
  }

  /**
   * Renders the VBO request assigning View.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The current request.
   *
   * @return array|null
   *   A renderable version of the assigning View.
   */
  public function assign(NodeInterface $node) {
    $output = [];

    $node_builder = $this->entityTypeManager()->getViewBuilder('node');
    $output['info'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Request being assigned'),
      'node' => $node_builder->view($node, 'teaser'),
    ];
    $output['view'] = views_embed_view('assign_volunteer', 'embed_1');

    return $output;
  }

  /**
   * Renders the review form with information about the request.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The request to assign to.
   * @param \Drupal\user\UserInterface $user
   *   The user to confirm as assignee.
   *
   * @return array|null
   *   A renderable version of the form with a summary of the request.
   */
  public function reviewVolunteer(NodeInterface $node, UserInterface $user) {
    $output = [];

    $message = '';
    $volunteers = $node->get('volunteers');
    foreach ($volunteers as $volunteer) {
      if ($volunteer->target_id === $user->id()) {
        $message = $volunteer->message;
      }
    }

    $user_builder = $this->entityTypeManager()->getViewBuilder('user');
    $output['user_info'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Volunteer being reviewed'),
      'node' => $user_builder->view($user, 'assign_info'),
    ];
    $output['message'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Volunteer message'),
      'message' => ['#markup' => $message],
    ];
    $node_builder = $this->entityTypeManager()->getViewBuilder('node');
    $output['request_info'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Request being assigned to'),
      'node' => $node_builder->view($node, 'teaser'),
    ];
    $output['form'] = $this->formBuilder()->getForm(VolunteerReviewForm::class, $node, $user);

    return $output;
  }

  /**
   * Title callback for volunteer review form.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The request to assign to.
   * @param \Drupal\user\UserInterface $user
   *   The user to confirm as assignee.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   A translated version of the title.
   */
  public function reviewVolunteerTitle(NodeInterface $node, UserInterface $user) {
    return $this->t('Are you sure you want to assign %user to request %request?', [
      '%user' => $user->label(),
      '%request' => $node->label(),
    ]);
  }

  /**
   * Renders the unassign form with information about the request.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The request to unassign from.
   * @param \Drupal\user\UserInterface $user
   *   The user to unassign.
   *
   * @return array|null
   *   A renderable version of the form with a summary of the request.
   */
  public function unassign(NodeInterface $node, UserInterface $user) {
    $output = [];

    $user_builder = $this->entityTypeManager()->getViewBuilder('user');
    $output['user_info'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('User being unassigned'),
      'node' => $user_builder->view($user, 'assign_info'),
    ];
    $node_builder = $this->entityTypeManager()->getViewBuilder('node');
    $output['request_info'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Request being unassigned from'),
      'node' => $node_builder->view($node, 'teaser'),
    ];
    $output['form'] = $this->formBuilder()->getForm(UnassignForm::class, $node, $user);

    return $output;
  }

  /**
   * Title callback for unassign form.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The request to unassign from.
   * @param \Drupal\user\UserInterface $user
   *   The user to unassign.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   A translated version of the title.
   */
  public function unassignTitle(NodeInterface $node, UserInterface $user) {
    return $this->t('Are you sure you want to unassign %user from %request? This cannot be undone.', [
      '%user' => $user->label(),
      '%request' => $node->label(),
    ]);
  }

  /**
   * Controls access to the log page.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The request.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Access result.
   */
  public function logAccess(NodeInterface $node) {
    return AccessResult::allowedIf($node->access('update'))
      ->andIf(AccessResult::allowedIf($node->bundle() === 'request'))
      ->addCacheableDependency($node);
  }

  /**
   * Assignment log.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The request.
   *
   * @return array
   *   Render array.
   */
  public function log(NodeInterface $node) {
    $messages = $this->entityTypeManager()->getStorage('message')
      ->loadByProperties(['template' => 'request_assignment', 'field_request' => $node->id()]);

    $build = [];

    $build['table'] = [
      '#type' => 'table',
      '#header' => [
        'created' => $this->t('Created'),
        'text' => $this->t('Text'),
        'author' => $this->t('Changed By'),
      ],
      '#title' => $this->t('Assignment Log'),
      '#rows' => [],
      '#empty' => $this->t('The log is empty'),
    ];
    foreach ($messages as $message) {
      $build['table']['#rows'][$message->id()] = [
        'changed' => $this->dateFormatter->format($message->getCreatedTime(), 'short'),
        'text' => ['data' => []],
        'author' => (!empty($message->getOwner())) ? $message->getOwner()
          ->label() : $this->t('Anonymous'),
      ];

      foreach ($message->getText() as $text) {
        $build['table']['#rows'][$message->id()]['text']['data'][] = ['#markup' => $text];
      }
    }

    return $build;
  }

}
