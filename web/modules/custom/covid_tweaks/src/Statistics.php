<?php

namespace Drupal\covid_tweaks;

use DateInterval;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryAggregateInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Statistics service.
 *
 * @todo: Add caching.
 */
class Statistics {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs a Statistics object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * Get the summary statistics.
   *
   * @return int[]
   *   An array with the following counts:
   *   - requests: The number of requests, excluding cancelled.
   *   - connections: The number of assigned or resolved requests.
   *   - resolved: The number of resolved requests.
   *   - offers: The number of offers of help.
   *   - volunteers: The number of active volunteers.
   */
  public function getSummaryStats(): array {
    $stats = [
      'requests' => 0,
      'connections' => 0,
      'resolved' => 0,
      'offers' => 0,
      'volunteers' => 0,
    ];

    // Build the request stats.
    $query = $this->entityTypeManager
      ->getStorage('node')
      ->getAggregateQuery();
    $query
      ->accessCheck(FALSE)
      ->condition('type', 'request')
      ->condition('field_status', 'cancelled', '!=');
    $query->aggregate('nid', 'COUNT');
    $query->groupBy('field_status');
    foreach ($query->execute() as $row) {
      $stats['requests'] += $row['nid_count'];
      if (in_array($row['field_status'], ['assigned', 'resolved'])) {
        $stats['connections'] += $row['nid_count'];
        if ($row['field_status'] === 'resolved') {
          $stats['resolved'] += $row['nid_count'];
        }
      }
    }

    // Get offer stats.
    $stats['offers'] = $this->entityTypeManager
      ->getStorage('profile')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', 'volunteer')
      ->count()
      ->execute();

    // Get volunteer stats.
    $stats['volunteers'] = $this->entityTypeManager
      ->getStorage('user')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('roles', 'verified_volunteer')
      ->count()
      ->execute();

    return $stats;
  }

  /**
   * Get volunteer summary stats grouped by source.
   *
   * @return array
   *   Keys are the source value, values are an array containing:
   *   - label: The source label.
   *   - offers: The number of offers.
   *   - volunteers: The number of verified volunteers.
   */
  public function getVolunteersBySource(): array {
    $sources = [];

    $definition = $this->entityFieldManager->getFieldStorageDefinitions('profile')['volunteer_source'];
    foreach ($definition->getSetting('allowed_values') as $value => $label) {
      $sources[$value] = [
        'label' => $label,
        'offers' => 0,
        'volunteers' => 0,
      ];
    }

    // Get offer numbers.
    $query = $this->getVolunteerBaseQuery();
    $query->exists('volunteer_source');
    $query->groupBy('volunteer_source');
    foreach ($query->execute() as $row) {
      $sources[$row['volunteer_source_value']]['offers'] = $row['profile_id_count'];
    }

    // Get verified volunteer numbers.
    $query = $this->entityTypeManager
      ->getStorage('user')
      ->getAggregateQuery();
    $query->accessCheck(FALSE);
    $query->condition('roles', 'verified_volunteer');
    $query->exists('profile_volunteer.entity.volunteer_source');
    $query->aggregate('uid', 'COUNT');
    $query->groupBy('profile_volunteer.entity.volunteer_source');
    foreach ($query->execute() as $row) {
      $sources[$row['volunteer_source_value']]['volunteers'] = $row['uid_count'];
    }

    return $sources;
  }

  /**
   * Get detailed counts for volunteer statues progress by week.
   *
   * @return array
   *   Outer keys are the week (Y-W), inner keys are the row key (status or
   *   status:source) OR 'date' for the header, and values the count.
   */
  public function getVolunteerProgress(string $group, bool $cumulative = TRUE): array {
    // Get the raw data, grouped by group_value and the relevant dates.
    $query = $this->getVolunteerBaseQuery();
    $query->groupBy($group);
    $query->groupBy('volunteer_submitted');
    $query->exists('volunteer_submitted');
    $query->aggregate('volunteer_submitted', 'MIN');
    $query->groupBy('volunteer_agreed');
    $query->aggregate('volunteer_agreed', 'MIN');
    $query->addTag('volunteer_group_date_by_week');
    $query->addMetaData('volunteer_group_date_by_week', [
      'volunteer_submitted',
      'volunteer_agreed',
    ]);
    $results = $query->execute();

    // Get the main property of the group field.
    $group_field_definition = $this->entityFieldManager
      ->getFieldStorageDefinitions('profile')[$group];
    $group_property = $group_field_definition->getMainPropertyName();

    // Compile the results into totals for each week/category.
    $week_data = [];
    $week_keys = [];
    foreach ($results as $row) {
      $group_value = $row["{$group}_{$group_property}"];
      $count = $row['profile_id_count'];

      // Submitted.
      $week_key = $row['volunteer_submitted_value'];
      $week_keys[$week_key] = $week_key;
      $this->setWeekData($week_data, $week_key, 'submitted', $group_value, $row['volunteer_submitted_min'], $count);

      // Deployed.
      if ($week_key = $row['volunteer_agreed_value']) {
        $week_keys[$week_key] = $week_key;
        $this->setWeekData($week_data, $week_key, 'onboarded', $group_value, $row['volunteer_agreed_min'], $count);
      }
    }

    // Add any missing weeks.
    $start = min($week_keys);
    $end = max($week_keys);
    /** @var \Drupal\Core\Datetime\DrupalDateTime $current */
    $current = $week_data[$start]['date'];
    do {
      $current->add(new DateInterval('P1W'));
      // MySQL has weeks starting from 1.
      $current_key = $current->format('Y-') . ($current->format('W') + 1);
      $week_data += [$current_key => ['date' => $current]];
    } while ($current_key < $end);

    // Sort the data by the week.
    ksort($week_data);

    // Ensure each week has each possible number.
    $profile = $this->entityTypeManager
      ->getStorage('profile')
      ->create(['type' => 'volunteer']);
    $groups = $group_field_definition
      ->getOptionsProvider($group, $profile)
      ->getPossibleValues();
    foreach ($week_data as &$week) {
      foreach (['submitted', 'onboarded'] as $status_value) {
        $week += [$status_value => 0];
        foreach ($groups as $group_value) {
          $week += ["{$status_value}:{$group_value}" => 0];
        }
      }
    }

    // If the data should be cumulative, loop over again and set.
    if ($cumulative) {
      $running_totals = [];
      foreach ($week_data as $week_key => &$week) {
        // Loop over and update the running totals.
        foreach ($week as $row_key => &$count) {
          // Skip the date label.
          if ($row_key === 'date') {
            continue;
          }

          $running_totals[$row_key] = ($running_totals[$row_key] ?? 0) + $count;

          // Decrement other counts if appropriate.
          $key_parts = explode(':', $row_key);
          $key_parts[] = NULL;
          [$status_key, $group_value] = $key_parts;
          if ($status_key === 'onboarded') {
            $adj_key = implode(':', array_filter(['submitted', $group_value]));
            $running_totals[$adj_key] = ($running_totals[$adj_key] ?? 0) - $count;
          }
        }

        // Loop over and set the values to be the running total.
        foreach ($week as $row_key => &$count) {
          // Skip the date label.
          if ($row_key === 'date') {
            continue;
          }
          $count = $running_totals[$row_key];
        }
      }
    }

    return $week_data;
  }

  /**
   * Set the data in the weeks array.
   *
   * @param array $week_data
   *   The weeks array.
   * @param string $week_key
   *   The key used for the week (Y-W).
   * @param string $row_key
   *   The row key for the count.
   * @param string|null $group
   *   The group for the count.
   * @param string $date
   *   A date within the week.
   * @param int $count
   *   The count to add.
   */
  protected function setWeekData(array &$week_data, string $week_key, string $row_key, ?string $group, string $date, int $count): void {
    if (!isset($week_data[$week_key])) {
      $week = DrupalDateTime::createFromFormat(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, $date);
      $week->setTime(12, 0, 0);
      $date_adj = $week->format('N') - 1;
      if ($date_adj) {
        $week->sub(new DateInterval("P{$date_adj}D"));
      }
      $week_data[$week_key] = [
        'date' => $week,
      ];
    }

    $week_data[$week_key][$row_key] = ($week_data[$week_key][$row_key] ?? 0) + $count;
    if ($group) {
      $week_data[$week_key]["{$row_key}:{$group}"] = ($week_data[$week_key]["{$row_key}:{$group}"] ?? 0) + $count;
    }
  }

  /**
   * Get the average volunteer processing time.
   *
   * @return int
   *   The number of seconds to process or NULL if there is no data.
   */
  public function getVolunteerProcessingTime(): ?int {
    $query = $this->getVolunteerBaseQuery();
    $query->condition('uid.entity.roles', 'verified_volunteer');
    $query->exists('volunteer_submitted');
    $query->exists('volunteer_agreed');
    $query->aggregate('volunteer_submitted', 'AVG');
    $query->aggregate('volunteer_agreed', 'AVG');
    $query->addTag('volunteer_date_to_timestamp');
    $query->addMetaData('volunteer_date_to_timestamp', [
      'volunteer_submitted_avg',
      'volunteer_agreed_avg',
    ]);
    $results = $query->execute();
    return $results ?
      (int) $results[0]['volunteer_agreed_avg'] - (int) $results[0]['volunteer_submitted_avg'] :
      NULL;
  }

  /**
   * Get the volunteer base query.
   *
   * @return \Drupal\Core\Entity\Query\QueryAggregateInterface
   *   The base query for volunteers.
   */
  public function getVolunteerBaseQuery(): QueryAggregateInterface {
    $query = $this->entityTypeManager
      ->getStorage('profile')
      ->getAggregateQuery();
    $query->accessCheck(FALSE);
    $query->condition('type', 'volunteer');
    $query->aggregate('profile_id', 'COUNT');
    return $query;
  }

}
