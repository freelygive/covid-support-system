<?php

/**
 * @file
 * Tokens.
 */

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Render\Markup;

/**
 * Implements hook_token_info().
 */
function covid_tweaks_token_info() {
  $type = [
    'name' => t('Covid Tweaks Tokens'),
    'description' => t('Tokens for Covid Tweaks.'),
    'needs-data' => 'message',
  ];

  $message['request-id'] = [
    'name' => t('Request ID'),
    'description' => t('Request ID'),
  ];

  $message['request-url'] = [
    'name' => t('Request URL'),
    'description' => t('Request URL'),
  ];

  $message['assignee-id'] = [
    'name' => t('Assignee ID'),
    'description' => t('Assignee User ID'),
  ];

  $message['assignee-name'] = [
    'name' => t('Assignee name'),
    'description' => t('Assignee User name'),
  ];

  $message['assignee-url'] = [
    'name' => t('Assignee ID'),
    'description' => t('Assignee User URL'),
  ];

  return [
    'types' => ['message' => $type],
    'tokens' => [
      'message' => $message,
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function covid_tweaks_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];
  if ($type == 'message' && !empty($data['message'])) {
    /** @var \Drupal\message\Entity\Message $message */
    $message = $data['message'];

    if ($message->bundle() == 'request_assignment') {
      /* @var \Drupal\node\Entity\Node $request */
      $request = $message->get('field_request')->entity;
      /* @var \Drupal\user\Entity\User $assignee */
      foreach ($tokens as $name => $original) {
        switch ($name) {
          case 'request-id':
            $replacements[$original] = $request->id();
            break;

          case 'request-url':
            $replacements[$original] = $request->toUrl('edit-form')->toString();
            break;

          case 'assignees':
            $assignees = [];

            foreach ($message->get('field_assignees') as $item) {
              /* @var \Drupal\user\Entity\User $assignee */
              $assignee = $item->entity;
              $assignees[] = $assignee->toLink()->toString();
            }

            $replacements[$original] = Markup::create(implode(', ', $assignees));
            break;

          case 'user-id':
            $replacements[$original] = $message->getOwnerId();
            break;

          case 'user-url':
            if ($owner = $message->getOwner()) {
              $replacements[$original] = $owner->toUrl()->toString();
            }
            break;

        }
      }
    }
  }

  return $replacements;
}
