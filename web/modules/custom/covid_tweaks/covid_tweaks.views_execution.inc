<?php

/**
 * @file
 * Views execution hooks.
 */

use Drupal\node\NodeInterface;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ViewExecutable;

/**
 * Implements hook_views_pre_build().
 */
function covid_tweaks_views_pre_build(ViewExecutable $view) {
  // Set the Location filter to the geolocation from the Request.
  if ($view->id() === 'assign_volunteer') {
    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node && $node instanceof NodeInterface && $node->bundle() == 'request') {
      $profile = $node->getOwner()->get('profile_crm_indiv');
      if (!$profile->isEmpty() && !$profile->entity->get('geolocation_geocoded')->isEmpty()) {
        $request_location = $profile->entity->get('geolocation_geocoded')->first()->getValue();
        if (!empty($request_location['lat']) && !empty($request_location['lon'])) {
          $view->filter['location_switch']->value['value'] = $request_location['lat'] . ',' . $request_location['lon'];
          // Arbitrarily high distance. We don't actually want to filter people
          // out but the sort requires a filter value.
          $view->filter['location_switch']->value['distance']['from'] = 2000;
        }
      }
    }
  }
  elseif ($view->id() === 'contact_request_directory') {
    $current_user = \Drupal::currentUser();
    if (!$current_user->hasPermission('manage covid requests')) {
      // Swap the fields for the rounded location.
      $filter = $view->filter['latlon'];
      $filter->realField =
        $filter->field =
        $filter->options['field'] = 'latlon_rounded';

      if ($view->current_display === 'block_1') {
        $field = $view->field['geolocation'];
        $field->realField =
          $field->definition['real field'] = 'entity:user/geolocation_rounded';
        $field->field =
          $field->options['field'] =
          $field->definition['field_name'] = 'geolocation_rounded';

        // If the user can't view/volunteer for requests, remove the name and
        // description popup.
        $options = &$view->style_plugin->options;
        if (!$current_user->hasPermission('view covid requests') && !$current_user->hasPermission('volunteer for covid requests')) {
          $options['name_field'] = '';
          $options['description_field'] = '';
        }
      }
    }
  }
}

/**
 * Implements hook_views_query_alter().
 */
function covid_tweaks_views_query_alter(ViewExecutable $view, QueryPluginBase $query) {
  if ($view->id() === 'profile_requests') {
    /** @var \Drupal\views\Plugin\views\query\Sql $query */

    // Create an OR group.
    $or_group = $query->setWhereGroup('OR');

    // Find the group member ID and move it to our new OR group.
    foreach ($query->where as $group => &$conditions) {
      foreach ($conditions['conditions'] as $key => $condition) {
        if ($condition['field'] === 'group_content_field_data_groups_field_data.entity_id = :group_content_field_data_entity_id') {
          unset($conditions['conditions'][$key]);
          $query->addWhere($or_group, $condition['field'], $condition['value'], $condition['operator']);
          break 2;
        }
      }
    }

    // Add a request has no group condition to the OR group.
    $query->addWhereExpression($or_group, 'group_content_field_data_node_field_data.id IS NULL');
  }
}

/**
 * Implements hook_views_pre_render().
 */
function covid_tweaks_views_pre_render(ViewExecutable $view) {
  // Convert the kilometer output to miles and add a suffix.
  if ($view->id() === 'assign_volunteer') {
    foreach ($view->result as $value) {
      $km = $value->{'entity:user/geolocation:location_switch__distance'}[0];
      $value->{'entity:user/geolocation:location_switch__distance'}[0] = ($km * 0.621371);
    }
    $view->field['location_switch_distance']->options['suffix'] = ' miles';
  }
}
