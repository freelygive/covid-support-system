<?php

/**
 * @file
 * Post update hooks for Covid Tweaks.
 */

use Drupal\Core\Site\Settings;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Fix the vulnerable typo in stored data.
 */
function covid_tweaks_post_update_fix_situation_vulnerable_typo_0_stored_values(&$sandbox) {
  $storage = \Drupal::entityTypeManager()->getStorage('profile');
  $query = $storage->getQuery()
    ->accessCheck(FALSE)
    ->allRevisions()
    ->condition('field_situation', 'vulerable');

  if (!isset($sandbox['progress'])) {
    $sandbox['progress'] = 0;
    $sandbox['last_id'] = 0;
    $sandbox['max'] = (clone $query)->count()->execute();
  }

  $query
    ->condition('revision_id', $sandbox['last_id'], '>')
    ->sort('revision_id')
    ->range(0, Settings::get('entity_update_batch_size'));

  $ids = $query->execute();
  /** @var \Drupal\profile\Entity\ProfileInterface $profile */
  foreach ($ids as $revision_id => $entity_id) {
    $profile = $storage->loadRevision($revision_id);
    $profile
      ->set('field_situation', 'vulnerable')
      ->save();
    $sandbox['progress']++;
    $sandbox['last_id'] = $revision_id;
  }
  $sandbox['#finished'] = empty($sandbox['max']) ?
    1 :
    $sandbox['progress'] / $sandbox['max'];
}

/**
 * Fix the vulnerable typo in the field allowed values.
 */
function covid_tweaks_post_update_fix_situation_vulnerable_typo_1_allowed_values() {
  // Get the field storage, if it exists.
  $field_storage = FieldStorageConfig::loadByName('profile', 'field_situation');
  if (!$field_storage) {
    return;
  }

  // Update the allowed values.
  $values = $field_storage->getSetting('allowed_values');
  $pos = array_search('vulerable', array_keys($values));
  if ($pos === FALSE) {
    return;
  }

  $values = array_slice($values, 0, $pos)
    + ['vulnerable' => 'Vulnerable']
    + array_slice($values, $pos + 1);

  $field_storage
    ->setSetting('allowed_values', $values)
    ->save();
}

/**
 * Fix stored data for requester/volunteer.
 */
function covid_tweaks_post_update_fix_request_volunteer_0_stored_values(&$sandbox) {
  $storage = \Drupal::entityTypeManager()->getStorage('profile');
  $query = $storage->getQuery()
    ->accessCheck(FALSE)
    ->allRevisions();
  $or = $query->orConditionGroup();
  $or->condition('field_are_you_looking_to_offer_o', 'I am looking to volunteer my support');
  $or->condition('field_are_you_looking_to_offer_o', 'I would like to request help');
  $query->condition($or);

  if (!isset($sandbox['progress'])) {
    $sandbox['progress'] = 0;
    $sandbox['last_id'] = 0;
    $sandbox['max'] = (clone $query)->count()->execute();
  }

  $query
    ->condition('revision_id', $sandbox['last_id'], '>')
    ->sort('revision_id')
    ->range(0, Settings::get('entity_update_batch_size'));

  $ids = $query->execute();
  /** @var \Drupal\profile\Entity\ProfileInterface $profile */
  foreach ($ids as $revision_id => $entity_id) {
    $profile = $storage->loadRevision($revision_id);

    if ($profile->get('field_are_you_looking_to_offer_o')->value == 'I would like to request help') {
      $profile->set('field_are_you_looking_to_offer_o', 'request');
    }
    elseif ($profile->get('field_are_you_looking_to_offer_o')->value == 'I am looking to volunteer my support') {
      $profile->set('field_are_you_looking_to_offer_o', 'volunteer');
    }
    $profile->save();
    $sandbox['progress']++;
    $sandbox['last_id'] = $revision_id;
  }
  $sandbox['#finished'] = empty($sandbox['max']) ?
    1 :
    $sandbox['progress'] / $sandbox['max'];
}

/**
 * Fix the allowed values for requester/volunteer.
 */
function covid_tweaks_post_update_fix_request_volunteer_1_allowed_values() {
  // Get the field storage, if it exists.
  $field_storage = FieldStorageConfig::loadByName('profile', 'field_are_you_looking_to_offer_o');
  if (!$field_storage) {
    return;
  }

  $field_storage
    ->setSetting('allowed_values', [
      'request' => 'I would like to request help',
      'volunteer' => 'I am looking to volunteer my support',
    ])
    ->save();
}
