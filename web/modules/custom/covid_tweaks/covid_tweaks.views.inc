<?php

/**
 * @file
 * Views hook implementations for the Covid tweaks module.
 */

use Drupal\field\FieldStorageConfigInterface;

/**
 * Implements hook_views_data_alter().
 *
 * Add custom entity operations links field.
 */
function covid_tweaks_views_data_alter(array &$data) {
  $data['node_field_data']['request_operations'] = [
    'title' => 'Request operations links',
    'help' => 'Provides links to perform entity operations (without the dropdown).',
    'real field' => 'nid',
    'field' => [
      'id' => 'covid_tweaks_entity_operations',
    ],
  ];

  // Use date popup plugin.
  $data['node__field_date']['field_date_value']['filter']['id'] = 'covid_date_popup';
  $data['node__field_date']['field_date_value']['filter']['format'] = 'date';
  $data['node_field_data']['created']['filter']['id'] = 'covid_date_popup';
  $data['node_field_data']['created']['filter']['format'] = 'date';

  // Use text for assignee as numeric breaks when value callback returns NULL.
  $data['node__field_assignees']['field_assignees_target_id']['filter']['id'] = 'standard';

  $data['views']['request_views_bulk_operations_bulk_form'] = [
    'title' => t('Views bulk operations (Request)'),
    'help' => t("Process entities returned by the view with Views Bulk Operations' actions."),
    'field' => [
      'id' => 'request_views_bulk_operations_bulk_form',
    ],
  ];
}

/**
 * Implements hook_field_views_data().
 */
function covid_tweaks_field_views_data(FieldStorageConfigInterface $field_storage) {
  $data = views_field_default_views_data($field_storage);

  // The code below only deals with the Entity reference field type.
  if ($field_storage->getType() != 'covid_tweaks_pending_volunteer') {
    return $data;
  }

  $entity_manager = \Drupal::entityManager();
  $entity_type_id = $field_storage->getTargetEntityTypeId();
  /** @var \Drupal\Core\Entity\Sql\DefaultTableMapping $table_mapping */
  $table_mapping = $entity_manager->getStorage($entity_type_id)->getTableMapping();

  foreach ($data as $table_name => $table_data) {
    // Add a relationship to the target entity type.
    $target_entity_type_id = $field_storage->getSetting('target_type');
    $target_entity_type = $entity_manager->getDefinition($target_entity_type_id);
    $entity_type_id = $field_storage->getTargetEntityTypeId();
    $entity_type = $entity_manager->getDefinition($entity_type_id);
    $target_base_table = $target_entity_type->getDataTable() ?: $target_entity_type->getBaseTable();
    $field_name = $field_storage->getName();

    // Provide a relationship for the entity type with the entity reference
    // field.
    $args = [
      '@label' => $target_entity_type->getLabel(),
      '@field_name' => $field_name,
    ];
    $data[$table_name][$field_name]['relationship'] = [
      'title' => t('@label referenced from @field_name', $args),
      'label' => t('@field_name: @label', $args),
      'group' => $entity_type->getLabel(),
      'help' => t('Appears in: @bundles.', ['@bundles' => implode(', ', $field_storage->getBundles())]),
      'id' => 'standard',
      'base' => $target_base_table,
      'entity type' => $target_entity_type_id,
      'base field' => $target_entity_type->getKey('id'),
      'relationship field' => $field_name . '_target_id',
    ];

    // Provide a reverse relationship for the entity type that is referenced by
    // the field.
    $args['@entity'] = $entity_type->getLabel();
    $args['@label'] = $target_entity_type->getLowercaseLabel();
    $pseudo_field_name = 'reverse__' . $entity_type_id . '__' . $field_name;
    $data[$target_base_table][$pseudo_field_name]['relationship'] = [
      'title' => t('@entity using @field_name', $args),
      'label' => t('@field_name', ['@field_name' => $field_name]),
      'group' => $target_entity_type->getLabel(),
      'help' => t('Relate each @entity with a @field_name set to the @label.', $args),
      'id' => 'entity_reverse',
      'base' => $entity_type->getDataTable() ?: $entity_type->getBaseTable(),
      'entity_type' => $entity_type_id,
      'base field' => $entity_type->getKey('id'),
      'field_name' => $field_name,
      'field table' => $table_mapping->getDedicatedDataTableName($field_storage),
      'field field' => $field_name . '_target_id',
      'join_extra' => [
        [
          'field' => 'deleted',
          'value' => 0,
          'numeric' => TRUE,
        ],
      ],
    ];

    $data[$table_name]["{$field_name}_message"]['real field'] = $field_name . '_target_id';
    $data[$table_name]["{$field_name}_message"]['field'] = [
      'id' => 'covid_tweaks_volunteer_message',
    ];

    $data[$table_name]["{$field_name}_review_link"] = [
      'real field' => $field_name . '_target_id',
      'field' => [
        'title' => t('Link to @entity @field_name review', $args),
        'group' => $entity_type->getLabel(),
        'help' => t('Provide a review link to the @field_name on @entity.', $args),
        'id' => 'covid_tweaks_request_review',
      ],
    ];
  }

  return $data;
}
