<?php

namespace Drupal\fg_layouts\Plugin\Layout;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Layout plugin for images.
 */
class ImageLayout extends DefaultConfigLayout {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    $config['height'] = 500;
    $config['size'] = 'cover';
    $config['position'] = '50%';
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $link = Link::fromTextAndUrl('here', Url::fromRoute('entity.media.collection', [], [
      'attributes' => ['target' => '_blank'],
    ]))->toString();
    $form['media'] = [
      '#title' => $this->t('Background Media'),
      '#description' => $this->t('You can add more media @here', [
        '@here' => $link,
      ]),
      '#type' => 'entity_autocomplete',
      '#target_type' => 'media',
      '#required' => TRUE,
      '#selection_handler' => 'default',
      '#selection_settings' => [
        'target_bundles' => ['image', 'video'],
      ],
    ];
    if (!empty($this->configuration['media'])) {
      $form['media']['#default_value'] = \Drupal::entityTypeManager()->getStorage('media')->load($this->configuration['media']);
    }
    $form['height'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Image height'),
      '#default_value' => $this->configuration['height'],
      '#required' => TRUE,
    ];

    $form['size'] = [
      '#type' => 'select',
      '#title' => $this->t('Image size'),
      '#options' => [
        'contain' => 'Contain',
        'cover' => 'Cover',
      ],
      '#default_value' => $this->configuration['size'],
      '#required' => TRUE,
    ];

    $form['position'] = [
      '#type' => 'select',
      '#title' => $this->t('Image position'),
      '#options' => [
        '25%' => 'Top',
        '50%' => 'Middle',
        '75%' => 'Bottom',
      ],
      '#default_value' => $this->configuration['position'],
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['media'] = $form_state->getValue('media');
    $this->configuration['height'] = $form_state->getValue('height');
    $this->configuration['size'] = $form_state->getValue('size');
    $this->configuration['position'] = $form_state->getValue('position');
  }

}
