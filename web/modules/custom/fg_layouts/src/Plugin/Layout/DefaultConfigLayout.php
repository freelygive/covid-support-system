<?php

namespace Drupal\fg_layouts\Plugin\Layout;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutDefault;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Configurable layout plugin.
 */
class DefaultConfigLayout extends LayoutDefault implements PluginFormInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'class' => '',
      'row_class' => '',
      'region_class' => '',
      'container' => FALSE,
      'color' => '',
      'menu_title' => '',
      'menu_link' => NULL,
      'menu_weight' => 0,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['class'] = [
      '#type' => 'textfield',
      '#title' => 'Wrapper Classes',
      '#default_value' => $this->configuration['class'],
    ];

    $form['row_class'] = [
      '#type' => 'textfield',
      '#title' => 'Row Classes',
      '#default_value' => $this->configuration['row_class'],
    ];

    $form['region_class'] = [
      '#type' => 'textfield',
      '#title' => 'Region Classes',
      '#default_value' => $this->configuration['region_class'],
    ];

    $form['container'] = [
      '#type' => 'checkbox',
      '#title' => 'Use Container',
      '#default_value' => $this->configuration['container'],
    ];

    $form['color'] = [
      '#type' => 'textfield',
      '#title' => 'Background colour',
      '#default_value' => $this->configuration['color'],
    ];

    $form['menu'] = [
      '#type' => 'fieldset',
      '#title' => 'Menu',
    ];

    $form['menu']['use_anchor'] = [
      '#type' => 'checkbox',
      '#title' => 'Add to menu',
      '#default_value' => !empty($this->configuration['menu_link']),
    ];

    $form['menu']['menu_title'] = [
      '#type' => 'textfield',
      '#title' => 'Menu Title',
      '#default_value' => $this->configuration['menu_title'],
      '#states' => [
        'visible' => [
          ':input[name="layout_settings[menu][use_anchor]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['menu']['menu_link'] = [
      '#type' => 'textfield',
      '#title' => 'Menu Link',
      '#default_value' => $this->configuration['menu_link'],
      '#states' => [
        'visible' => [
          ':input[name="layout_settings[menu][use_anchor]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['menu']['menu_weight'] = [
      '#type' => 'textfield',
      '#title' => 'Menu Weight',
      '#default_value' => $this->configuration['menu_weight'],
      '#states' => [
        'visible' => [
          ':input[name="layout_settings[menu][use_anchor]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $weight = $form_state->getValue(['menu', 'menu_weight']);
    if (!empty($weight) && !is_numeric($weight)) {
      $form_state->setError($form['menu']['menu_weight'], $this->t('The weight must be numeric'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['class'] = $form_state->getValue('class');
    $this->configuration['row_class'] = $form_state->getValue('row_class');
    $this->configuration['region_class'] = $form_state->getValue('region_class');
    $this->configuration['container'] = $form_state->getValue('container');
    $this->configuration['color'] = $form_state->getValue('color');

    $use_anchor = $form_state->getValue(['menu', 'use_anchor']);
    $this->configuration['menu_title'] = $use_anchor ? $form_state->getValue(['menu', 'menu_title']) : NULL;
    $this->configuration['menu_link'] = $use_anchor ? $form_state->getValue(['menu', 'menu_link']) : NULL;
    $this->configuration['menu_weight'] = $use_anchor ? $form_state->getValue(['menu', 'menu_weight']) : NULL;
  }

}
