<?php

namespace Drupal\fg_layouts\Plugin\Layout;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a default class for Layout plugins with a mobile sidebar.
 */
class Layout75 extends DefaultConfigLayout {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();

    $config['col2_show_mobile'] = FALSE;
    $config['col2_first_mobile'] = FALSE;

    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['col2_show_mobile'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show second column on mobile'),
      '#default_value' => $this->configuration['col2_show_mobile'],
    ];

    $form['col2_first_mobile'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show second column first on mobile'),
      '#default_value' => $this->configuration['col2_first_mobile'],
      '#states' => [
        'visible' => [
          ':input[name="layout_settings_wrapper[layout_settings][col2_show_mobile]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // If column 2 is hidden on mobile then it can't be shown first.
    if ($form_state->getValue('col2_show_mobile') == FALSE) {
      $form_state->setValue('col2_first_mobile', FALSE);
    }

    parent::submitConfigurationForm($form, $form_state);

    $this->configuration['col2_show_mobile'] = $form_state->getValue('col2_show_mobile');
    $this->configuration['col2_first_mobile'] = $form_state->getValue('col2_first_mobile');
  }

}
