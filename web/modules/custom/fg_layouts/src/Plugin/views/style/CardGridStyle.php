<?php

namespace Drupal\fg_layouts\Plugin\views\style;

use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * Card Grid style plugin to render rows in a masonry grid.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "card_grid",
 *   title = @Translation("Card grid"),
 *   help = @Translation("Displays rows in a masonry grid."),
 *   theme = "views_view_card_grid",
 *   display_types = {"normal"}
 * )
 *
 * @see https://css-tricks.com/piecing-together-approaches-for-a-css-masonry-layout/#article-header-id-5
 * @see https://codepen.io/andybarefoot/pen/QMeZda
 */
class CardGridStyle extends StylePluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

}
