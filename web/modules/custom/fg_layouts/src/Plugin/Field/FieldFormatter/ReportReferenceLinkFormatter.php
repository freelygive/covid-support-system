<?php

namespace Drupal\fg_layouts\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\Exception\UndefinedLinkTemplateException;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceLabelFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'entity reference label' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_reference_report_link",
 *   label = @Translation("Reports Link"),
 *   description = @Translation("Display the label of the referenced entities."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class ReportReferenceLinkFormatter extends EntityReferenceLabelFormatter {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->getSetting('link') ? t('Link to the reports page') : t('No link');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'color' => 'primary',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['color'] = [
      '#title' => t('Color'),
      '#type' => 'select',
      '#options' => [
        'primary' => 'Primary',
        'secondary' => 'Secondary',
        'success' => 'Success',
        'info' => 'Info',
        'warning' => 'Warning',
        'danger' => 'Danger',
        'teal' => 'Teal',
        'blue' => 'Blue',
        'blue-light' => 'Light Blue',
        'light' => 'Light',
        'dark' => 'Dark',
      ],
      '#default_value' => $this->getSetting('color'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $output_as_link = $this->getSetting('link');

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
      $label = $entity->label();
      // If the link is to be displayed and the entity has a uri, display a
      // link.
      if ($output_as_link && !$entity->isNew()) {
        try {
          $map = [
            'distribution_lists' => 'distribution_lists',
            'key_companies' => 'companies',
            'report_thematic_interests' => 'themes',
            'sectors' => 'sectors',
          ];

          if (!in_array($items->getName(), array_keys($map))) {
            $output_as_link = FALSE;
          }
          else {
            $output_as_link = FALSE;
            $key = 'f[0]';
            $options = ['query' => [$key => $map[$items->getName()] . ':' . $entity->id()]];
//            $uri = Url::fromRoute('view.FG_subscription_reports.page_1', [], $options);
          }
        }
        catch (UndefinedLinkTemplateException $e) {
          // This exception is thrown by \Drupal\Core\Entity\Entity::urlInfo()
          // and it means that the entity type doesn't have a link template nor
          // a valid "uri_callback", so don't bother trying to output a link for
          // the rest of the referenced entities.
          $output_as_link = FALSE;
        }
      }

      if ($output_as_link && isset($uri) && !$entity->isNew()) {
        $elements[$delta] = [
          '#type' => 'link',
          '#title' => $label,
          '#url' => $uri,
          '#options' => $uri->getOptions(),
          '#attributes' => ['class' => ['badge badge-pill']],
        ];
        $elements[$delta]['#attributes']['class'][] = 'badge-' . $this->getSetting('color');

        if (!empty($items[$delta]->_attributes)) {
          $elements[$delta]['#options'] += ['attributes' => []];
          $elements[$delta]['#options']['attributes'] += $items[$delta]->_attributes;
          // Unset field item attributes since they have been included in the
          // formatter output and shouldn't be rendered in the field template.
          unset($items[$delta]->_attributes);
        }
      }
      else {
        $elements[$delta] = [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#value' => Markup::create(Html::escape($label)),
          '#attributes' => ['class' => ['badge badge-pill badge']],
        ];
        $elements[$delta]['#attributes']['class'][] = 'badge-' . $this->getSetting('color');
      }
      $elements[$delta]['#cache']['tags'] = $entity->getCacheTags();
    }

    return $elements;
  }

}
