<?php

namespace Drupal\fg_layouts\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a Block type that exports settings to configuration.
 *
 * @Block(
 *   id = "fg_layouts_block",
 *   admin_label = @Translation("Layout block"),
 *   category = @Translation("Inline blocks"),
 * )
 */
class LayoutBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    if (!empty($form['id']['#default_value'])) {
      $form['id']['#attributes']['disabled'] = 'disabled';
      $form['id']['#attributes']['readonly'] = 'readonly';
    }

    $form['content'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Content'),
      '#default_value' => !empty($this->configuration['content']) ? $this->configuration['content'] : NULL,
      '#format' => !empty($this->configuration['format']) ? $this->configuration['format'] : NULL,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    $values = $form_state->getValues();
    $this->configuration['content'] = $values['content']['value'];
    $this->configuration['format'] = $values['content']['format'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $output = [];

    $block_config = $this->getConfiguration();

    $output['content'] = [
      '#type' => 'processed_text',
      '#text' => !empty($block_config['content']) ? $block_config['content'] : NULL,
      '#format' => !empty($block_config['format']) ? $block_config['format'] : NULL,
    ];

    return $output;
  }

}
