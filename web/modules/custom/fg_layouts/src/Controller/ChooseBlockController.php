<?php

// phpcs:ignoreFile

namespace Drupal\fg_layouts\Controller;

use Drupal\Core\Ajax\AjaxHelperTrait;
use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\layout_builder\Context\LayoutBuilderContextTrait;
use Drupal\layout_builder\SectionStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a controller to choose a new block.
 */
class ChooseBlockController implements ContainerInjectionInterface {

  use AjaxHelperTrait;
  use LayoutBuilderContextTrait;

  /**
   * The block manager.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $blockManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $db;

  /**
   * UUIDs for all team-member blocks in the system.
   *
   * @var array
   */
  protected $teamMemberBlockUuids;

  /**
   * ChooseBlockController constructor.
   *
   * @param \Drupal\Core\Block\BlockManagerInterface $block_manager
   *   The block manager.
   * @param \Drupal\Core\Extension\ModuleHandler $module_handler
   *   The module handler.
   * @param \Drupal\Core\Database\Connection $db
   *   Database connection.
   */
  public function __construct(BlockManagerInterface $block_manager, ModuleHandler $module_handler, Connection $db) {
    $this->blockManager = $block_manager;
    $this->moduleHandler = $module_handler;
    $this->db = $db;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.block'),
      $container->get('module_handler'),
      $container->get('database')
    );
  }

  /**
   * Provides the UI for choosing a new block.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   * @param int $delta
   *   The delta of the section to splice.
   * @param string $region
   *   The region the block is going in.
   *
   * @return array
   *   A render array.
   */
  public function build(SectionStorageInterface $section_storage, $delta, $region) {
    $build['#type'] = 'container';
    $build['#attributes']['class'][] = 'block-categories';

    // @todo Explicitly cast delta to an integer, remove this in
    //   https://www.drupal.org/project/drupal/issues/2984509.
    $delta = (int) $delta;

    $definitions = $this->blockManager->getFilteredDefinitions('layout_builder', $this->getAvailableContexts($section_storage), [
      'section_storage' => $section_storage,
      'delta' => $delta,
      'region' => $region,
    ]);

    $allowed_block_types = [
      'block_content',
      'extra_field_block',
      'field_block',
      'inline_block',
      'views_block',
      'views_exposed_filter_block',
      'clinks_annual_review_dynamic_menu_block',
      'clinks_membership_grantnet_block',
      'clinks_tweaks_recent_content_block',
      'fg_layouts_block',
      'nw_layouts_block',
    ];

    // Alter hook to add allowed block types.
    $this->moduleHandler->alter('fg_layouts_offcanvas_allowed_block_types', $allowed_block_types);

    $allowed_views_categories = [
      'Lists (Views)',
    ];

    // Alter hook to add allowed views categories.
    $this->moduleHandler->alter('fg_layouts_offcanvas_allowed_views_categories', $allowed_views_categories);

    // Do not allow Custom image or Custom text inline blocks to be used as
    // their content is not stored in config and may be lost when the layout
    // config is next imported.
    $blacklisted_block_ids = [
      'inline_block:basic',
      'inline_block:media',
    ];

    foreach ($this->blockManager->getGroupedDefinitions($definitions) as $category => $blocks) {
      $add_blocks = [];
      foreach ($blocks as $block_id => $block) {
        if (in_array($block_id, $blacklisted_block_ids)) {
//          continue;
        }

        list($base) = explode(':', $block_id);

        // Special case for 'team member' blocks.
        // These are of the type 'block_content', with a category of 'custom'
        // but we don't want to display everything in this category.
        // Instead create a pseudo-category of "Team Members", which we allow,
        // but only on the team member page.
        if ($section_storage->label() == 'Meet the team' && $base == 'block_content' && $this->isTeamMemberBlock($block_id)) {
          $category = 'Team Members';
          // Make sure the team members category is always first in the list
          // when we're on the team page.
          $build[$category]['#weight'] = -1;
        }
        elseif (!in_array($base, $allowed_block_types)) {
//          continue;
        }

        if ($base == 'views_block') {
          if (!in_array($category, $allowed_views_categories)) {
//            continue;
          }
        }

        $link = [
          'title' => $block['admin_label'],
          'url' => Url::fromRoute('layout_builder.add_block',
            [
              'section_storage_type' => $section_storage->getStorageType(),
              'section_storage' => $section_storage->getStorageId(),
              'delta' => $delta,
              'region' => $region,
              'plugin_id' => $block_id,
            ]
          ),
        ];
        if ($this->isAjax()) {
          $link['attributes']['class'][] = 'use-ajax';
          $link['attributes']['data-dialog-type'][] = 'dialog';
          $link['attributes']['data-dialog-renderer'][] = 'off_canvas';
        }
        $add_blocks[] = $link;
      }

      if (!empty($add_blocks)) {
        $build[$category]['#type'] = 'details';
        $build[$category]['#open'] = TRUE;
        $build[$category]['#title'] = $category;
        $build[$category]['links'] = [
          '#theme' => 'links',
        ];
        $build[$category]['links']['#links'] = $add_blocks;
      }
    }

    return $build;
  }

  /**
   * Checks if the block is a team member block.
   *
   * @param string $block_id
   *   The ID if the block.
   *
   * @return bool
   *   Whether the block is a team member block.
   */
  protected function isTeamMemberBlock($block_id) {
    if (strpos($block_id, 'block_content:') == 0) {
      // Must start with block_content:
      list($prefix, $block_id) = explode(':', $block_id);

      if (!isset($this->teamMemberBlockUuids)) {
        $query = $this->db->select('block_content', 'b');
        $query->addField('b', 'uuid');
        $query->condition('type', 'team');
        $this->teamMemberBlockUuids = $query->execute()->fetchCol(0);
      }

      return in_array($block_id, $this->teamMemberBlockUuids);
    }
    return FALSE;
  }

}
