<?php

namespace Drupal\fg_layouts\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('layout_builder.choose_section')) {
      $defaults = $route->getDefaults();
      $defaults['_controller'] = '\Drupal\fg_layouts\Controller\ChooseSectionController::build';
      $route->setDefaults($defaults);
    }

    if ($route = $collection->get('layout_builder.choose_block')) {
      $defaults = $route->getDefaults();
      $defaults['_controller'] = '\Drupal\fg_layouts\Controller\ChooseBlockController::build';
      $route->setDefaults($defaults);
    }
  }

}
